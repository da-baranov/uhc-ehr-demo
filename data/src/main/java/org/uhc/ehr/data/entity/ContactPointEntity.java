package org.uhc.ehr.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Table(name = "ContactPoints")
@Entity
@Getter
@Setter
public class ContactPointEntity {
    @Id
    @Column
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    UUID uuid;

    @Enumerated(EnumType.STRING)
    ContactPointSystem system = ContactPointSystem.Phone;

    @Enumerated(EnumType.STRING)
    ContactPointUse use = ContactPointUse.Mobile;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(nullable = false, name = "PersonUuid")
    PersonEntity person;

    @Column(name = "Value_")
    String value;
}
