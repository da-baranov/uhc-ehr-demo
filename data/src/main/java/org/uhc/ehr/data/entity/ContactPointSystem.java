package org.uhc.ehr.data.entity;

public enum ContactPointSystem {
    Phone,
    Fax,
    Email,
    Pager,
    Url,
    Sms,
    Other
}
