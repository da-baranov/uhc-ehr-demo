package org.uhc.ehr.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "Patients")
public final class PatientEntity extends PersonEntity {
    String note;
}
