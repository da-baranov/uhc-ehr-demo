package org.uhc.ehr.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uhc.ehr.data.entity.CodeSystemVersionEntity;

import java.util.UUID;

public interface CodeSystemVersionRepository extends JpaRepository<CodeSystemVersionEntity, UUID> {

    CodeSystemVersionEntity findByCodeSystemUuidAndVersion(UUID codeSystemId, String version);
}
