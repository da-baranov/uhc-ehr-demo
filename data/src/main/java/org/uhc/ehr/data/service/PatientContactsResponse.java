package org.uhc.ehr.data.service;

import org.uhc.ehr.data.dto.ContactPoint;

public class PatientContactsResponse extends GenericResponse<ContactPoint> {
}
