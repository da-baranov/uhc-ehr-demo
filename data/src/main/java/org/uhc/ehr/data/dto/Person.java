package org.uhc.ehr.data.dto;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.uhc.ehr.data.Util;

import javax.persistence.Basic;
import javax.persistence.FetchType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Calendar;
import java.util.UUID;

@Getter
@Setter
public class Person {
    @Schema(description = "Record identifier")
    UUID uuid;

    @Schema(description = "Logical id of the patient (e.g. SSN)")
    String id;

    @Schema(description = "Whether this patient's record is in active use")
    Boolean active = Boolean.TRUE;

    @Schema(description = "Family name (often called 'Surname')", required = true)
    @NotBlank(message = "Family name is requred")
    @NotNull(message = "Family name is requred")
    @Size(min = 2, message = "Family name must be at least 2 characters long")
    String familyName;

    @Schema(description = "First given name")
    String firstName;

    @Schema(description = "Middle given name")
    String lastName;

    @Schema(description = "The gender of a person used for administrative purposes")
    String gender;

    @Schema(description = "The date of birth for the individual")
    Calendar birthDate;

    @Schema(description = "Indicates if the individual is deceased or not")
    Calendar deceased;

    @Schema(description = "Patient age (in years)")
    @JsonGetter("age")
    public Integer getAge() {
        if (birthDate == null) return null;
        return Util.getDiffYears(birthDate, Calendar.getInstance());
    }
}
