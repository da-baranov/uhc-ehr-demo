package org.uhc.ehr.data.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.uhc.ehr.data.entity.ContactPointSystem;
import org.uhc.ehr.data.entity.ContactPointUse;

import java.util.UUID;

@Getter
@Setter
public class ContactPoint {

    @Schema(description = "Record identifier")
    UUID uuid;

    @Schema(description = "Purpose of this contact point")
    ContactPointUse use = ContactPointUse.Mobile;

    @Schema(description = "Telecommunications form for contact point")
    ContactPointSystem system = ContactPointSystem.Phone;

    @Schema(description = "The actual contact point details", required = true)
    String value;
}
