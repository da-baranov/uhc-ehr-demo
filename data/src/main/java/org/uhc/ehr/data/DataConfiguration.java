package org.uhc.ehr.data;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.envers.repository.support.EnversRevisionRepositoryFactoryBean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories(repositoryFactoryBeanClass = EnversRevisionRepositoryFactoryBean.class, basePackages = {"org.uhc.ehr.data"})
@EntityScan(basePackages = {"org.uhc.ehr.data", "org.uhc.ehr.data.dto"})
@ComponentScan(basePackages = {"org.uhc.ehr.data"})
public class DataConfiguration {
}
