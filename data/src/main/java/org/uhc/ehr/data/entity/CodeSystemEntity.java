package org.uhc.ehr.data.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Table(name = "CodeSystems",
        uniqueConstraints = {
                @UniqueConstraint(name = "c_code_system_url_uk", columnNames = {"Url"}),
                @UniqueConstraint(name = "c_code_system_name_uk", columnNames = {"Name"})
        })
@Entity
@Getter
@Setter
public class CodeSystemEntity {
    @Id
    @Column
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    UUID uuid;

    @Column(nullable = false)
    String url;

    String title;

    @Column(nullable = false)
    String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "codeSystem")
    Set<CodeSystemVersionEntity> versions = new HashSet<>();
}
