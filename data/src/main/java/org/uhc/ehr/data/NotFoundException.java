package org.uhc.ehr.data;

import java.util.UUID;

public class NotFoundException extends DataException {

    public NotFoundException(Class<?> clazz, UUID id) {
        super("Record of type " + clazz.getName() + " with id " + id + " not found.");
    }
}
