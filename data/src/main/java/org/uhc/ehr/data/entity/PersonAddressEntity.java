package org.uhc.ehr.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Table(name = "PersonAddresses")
@Entity
public class PersonAddressEntity {

    @Id
    @Column
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    UUID uuid;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(nullable = false, name = "PersonUuid")
    PersonEntity person;

    @Audited
    @Enumerated(EnumType.STRING)
    AddressUse use;

    @Audited
    @Enumerated(EnumType.STRING)
    AddressType type;

    @Audited
    String text;

    @Audited
    @ElementCollection
    @CollectionTable(name = "AddressLines", joinColumns = {@JoinColumn(name = "AddressUuid")})
    @OrderColumn(name = "position")
    List<String> line = new ArrayList<>();

    @Audited
    String city;

    @Audited
    String district;

    @Audited
    String state;

    @Audited
    String postalCode;

    @Audited
    String country;
}
