package org.uhc.ehr.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.uhc.ehr.data.entity.CodeSystemEntity;
import org.uhc.ehr.data.entity.CodeSystemVersionEntity;
import org.uhc.ehr.data.entity.CodingEntity;

import java.util.Comparator;
import java.util.UUID;

@Repository
public interface CodeSystemRepository extends JpaRepository<CodeSystemEntity, UUID> {

    CodeSystemEntity findByUrl(String url);

    default CodingEntity findCoding(String url, String code) {
        return findCoding(url, code, null);
    }

    default CodingEntity findCoding(String url, String code, String version) {

        url = "" + url;
        code = "" + code;

        var codeSystem = findByUrl(url);
        if (codeSystem == null) return null;

        CodeSystemVersionEntity codeSystemVersion;

        if (version != null) {
            codeSystemVersion =
                    codeSystem
                            .getVersions()
                            .stream()
                            .filter(row -> version.equals(row.getVersion()))
                            .findFirst()
                            .orElse(null);
        } else {
            var c = new Comparator<CodeSystemVersionEntity>() {
                @Override
                public int compare(CodeSystemVersionEntity h1, CodeSystemVersionEntity h2) {
                    return Runtime.Version.parse(h1.getVersion()).compareTo(Runtime.Version.parse(h2.getVersion()));
                }
            };
            codeSystemVersion = codeSystem
                    .getVersions()
                    .stream()
                    .max(c)
                    .orElse(null);
        }

        if (codeSystemVersion == null) return null;

        String finalCode = code;
        return codeSystemVersion
                .getCodings()
                .stream()
                .filter(row -> finalCode.equals(row.getCode()))
                .findFirst()
                .orElse(null);
    }
}
