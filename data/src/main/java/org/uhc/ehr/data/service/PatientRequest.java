package org.uhc.ehr.data.service;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
public class PatientRequest {

    @Schema(description = "Search by person identifier. Wildcard * is allowed.")
    String id;

    @Schema(description = "Search by person family name. Wildcard * is allowed.")
    String familyName;

    @Schema(description = "Search by person first name. Wildcard * is allowed.")
    String firstName;

    @Schema(description = "Search by person last name. Wildcard * is allowed.")
    String lastName;

    @Schema(description = "The date of birth")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    LocalDateTime birthDateFrom;

    @Schema(description = "The date of birth")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    LocalDateTime birthDateTo;

    @Schema(description = "The count query parameter is used to include in a first page of a response only the first N items of a resource collection.")
    int _count = 50;

    @Schema(description = "The limit query parameter is used to exclude from a response the first N items of a resource collection.")
    int _offset = 0;

    String _sort;
}
