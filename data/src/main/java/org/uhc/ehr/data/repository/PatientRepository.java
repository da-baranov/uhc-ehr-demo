package org.uhc.ehr.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.stereotype.Repository;
import org.uhc.ehr.data.entity.PatientEntity;

import java.util.UUID;

@Repository
public interface PatientRepository
        extends
        JpaRepository<PatientEntity, UUID>,
        RevisionRepository<PatientEntity, UUID, Integer> {

    PatientEntity findById(String id);
}
