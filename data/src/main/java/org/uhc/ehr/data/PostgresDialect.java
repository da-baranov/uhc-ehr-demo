package org.uhc.ehr.data;

import java.sql.Types;

public final class PostgresDialect extends org.hibernate.dialect.PostgreSQL10Dialect {

    public PostgresDialect() {
        // there are no performance differences on Postgres between varchar(n) and text
        this.registerColumnType(Types.VARCHAR, "text");
        this.registerColumnType(Types.TIMESTAMP, "timestamp with time zone");
    }
}