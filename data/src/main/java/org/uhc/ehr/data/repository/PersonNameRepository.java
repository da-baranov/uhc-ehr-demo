package org.uhc.ehr.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.uhc.ehr.data.entity.HumanNameEntity;

import java.util.UUID;

@Repository
public interface PersonNameRepository extends JpaRepository<HumanNameEntity, UUID> {
}
