package org.uhc.ehr.data.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Table(name = "Practitioners")
@Entity
@Getter
@Setter
public final class PractitionerEntity extends PersonEntity {
}
