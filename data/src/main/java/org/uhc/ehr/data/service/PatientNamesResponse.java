package org.uhc.ehr.data.service;

import org.uhc.ehr.data.dto.HumanName;

public class PatientNamesResponse extends GenericResponse<HumanName> {
}
