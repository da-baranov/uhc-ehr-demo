package org.uhc.ehr.data.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "Demographics and other administrative information about an individual or animal receiving care or other health-related services.")
public class Patient extends Person {
}
