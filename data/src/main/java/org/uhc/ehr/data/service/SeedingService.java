package org.uhc.ehr.data.service;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.uhc.ehr.data.dto.ContactPoint;
import org.uhc.ehr.data.dto.Patient;
import org.uhc.ehr.data.dto.PersonAddress;
import org.uhc.ehr.data.entity.*;
import org.uhc.ehr.data.repository.CodeSystemRepository;
import org.uhc.ehr.data.repository.CodeSystemVersionRepository;

import javax.persistence.EntityManager;
import java.util.Calendar;
import java.util.HashMap;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@Service
public class SeedingService {

    @Autowired
    CodeSystemRepository codeSystemRepository;
    @Autowired
    CodeSystemVersionRepository codeSystemVersionRepository;
    @Autowired
    PatientService patientService;
    @Autowired
    EntityManager entityManager;

    public void seedDictionaries() {
        String url = "http://hl7.org/fhir/ValueSet/administrative-gender";
        String version = "4.3.0";
        var codeSystemGender = codeSystemRepository.findByUrl(url);
        if (codeSystemGender == null) {
            codeSystemGender = new CodeSystemEntity();
            codeSystemGender.setUrl(url);
            codeSystemGender.setName("AdministrativeGender");
            codeSystemGender.setTitle("AdministrativeGender");
            codeSystemRepository.save(codeSystemGender);
        }
        var codeSystemGenderVersion = codeSystemVersionRepository.findByCodeSystemUuidAndVersion(codeSystemGender.getUuid(), version);
        if (codeSystemGenderVersion == null) {
            codeSystemGenderVersion = new CodeSystemVersionEntity();
            codeSystemGenderVersion.setVersion(version);
            codeSystemGender.getVersions().add(codeSystemGenderVersion);
            codeSystemGenderVersion.setCodeSystem(codeSystemGender);
            codeSystemVersionRepository.save(codeSystemGenderVersion);
        }
        var genderCodes = new HashMap<String, String>();
        genderCodes.put("male", "Male");
        genderCodes.put("female", "Female");
        genderCodes.put("other", "Other");
        genderCodes.put("unknown", "Unknown");
        for (var key : genderCodes.keySet()) {
            var value = genderCodes.get(key);
            var coding = codeSystemGenderVersion
                    .getCodings()
                    .stream()
                    .filter(row -> key.equals(row.getCode()))
                    .findFirst()
                    .orElse(null);
            if (coding == null) {
                coding = new CodingEntity();
                coding.setCode(key);
                coding.setDisplay(value);
                codeSystemGenderVersion.getCodings().add(coding);
                coding.setCodeSystemVersion(codeSystemGenderVersion);
            }
        }
        codeSystemVersionRepository.save(codeSystemGenderVersion);
    }

    public void seedPatients(int count) {
        var faker = new Faker();

        for (int i = 0; i < count; i++) {
            var p = new Patient();
            p.setId(faker.idNumber().ssnValid());
            if (i % 2 == 0) p.setGender("male");
            else p.setGender("female");

            // Birthdate
            var bd = Calendar.getInstance();
            bd.setTime(faker.date().birthday());
            p.setBirthDate(bd);

            // Name
            p.setFamilyName(faker.name().lastName());
            p.setFirstName(faker.name().firstName());

            // Patient
            p = patientService.insertPatient(p);

            // Contact - tel
            var tel = new ContactPoint();
            tel.setValue(faker.phoneNumber().cellPhone());
            patientService.insertPatientContact(p.getUuid(), tel);

            // Contact - email
            var emal = new ContactPoint();
            emal.setUse(ContactPointUse.Work);
            emal.setSystem(ContactPointSystem.Email);
            emal.setValue(faker.internet().emailAddress());
            patientService.insertPatientContact(p.getUuid(), emal);

            // Address
            var address = new PersonAddress();
            address.setCountry(faker.address().country());
            address.setCity(faker.address().city());
            address.setLine(faker.address().streetAddress(true));
            address.setPostalCode(faker.address().zipCode());
            address.setUse(AddressUse.Home);
            address.setState(faker.address().state());
            patientService.insertPatientAddress(p.getUuid(), address);
        }
    }

    public void deleteAllPatients() {
        patientService.deleteAll();
    }

    public void seed() {
        seedDictionaries();
        seedPatients(1000);
    }
}
