package org.uhc.ehr.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Table(name = "CodeSystemVersions",
        uniqueConstraints = {
                @UniqueConstraint(name = "c_code_system_version_uk", columnNames = {"CodeSystemUuid", "Version"})
        })
@Entity
@Getter
@Setter
public class CodeSystemVersionEntity {
    @Id
    @Column
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    UUID uuid;

    @Column(nullable = false)
    String version;

    @ManyToOne
    @JoinColumn(nullable = false, name = "CodeSystemUuid")
    @JsonIgnore
    CodeSystemEntity codeSystem;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "codeSystemVersion")
    Set<CodingEntity> codings = new HashSet<>();
}
