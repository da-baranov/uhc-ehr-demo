package org.uhc.ehr.data.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.uhc.ehr.data.entity.HumanNameUse;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class HumanName {

    @Schema(description = "Record identifier")
    UUID uuid;

    @Schema(description = "Family name (often called 'Surname')", required = true)
    @NotBlank(message = "Family name is requred")
    @NotNull(message = "Family name is requred")
    @Size(min = 2, message = "Family name must be at least 2 characters long")
    String familyName;

    @Schema(description = "First given name")
    String firstName;

    @Schema(description = "Middle given name")
    String lastName;

    @Schema(description = "A name part that comes before the name")
    String prefix;

    @Schema(description = "A name part that comes after the name")
    String suffix;

    @Schema(description = "The use of a human name")
    HumanNameUse use;
}
