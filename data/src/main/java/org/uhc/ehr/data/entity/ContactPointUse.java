package org.uhc.ehr.data.entity;

public enum ContactPointUse {
    Home,
    Work,
    Temp,
    Old,
    Mobile
}
