package org.uhc.ehr.data.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.uhc.ehr.data.entity.AddressType;
import org.uhc.ehr.data.entity.AddressUse;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@Schema(description = "An address expressed using postal conventions (as opposed to GPS or other location definition formats). This data type may be used to convey addresses for use in delivering mail as well as for visiting locations which might not be valid for mail delivery. There are a variety of postal address formats defined around the world.")
public class PersonAddress {

    @Schema(description = "Record identifier")
    UUID uuid;

    @Schema(description = "Name of city, town etc.")
    String city;

    @Schema(description = "Country")
    @NotNull
    String country;

    @Schema(description = "District name (aka county)")
    String district;

    @Schema(description = "Street name, number, direction etc.")
    @NotNull
    String line;

    @Schema(description = "Postal code for area")
    String postalCode;

    @Schema(description = "Sub-unit of country")
    String state;

    @Schema(description = "Text representation of the address")
    String text;

    @Schema(description = "The type of an address (physical/postal)")
    AddressType type;

    @Schema(description = "The use of an address")
    AddressUse use = AddressUse.Home;
}
