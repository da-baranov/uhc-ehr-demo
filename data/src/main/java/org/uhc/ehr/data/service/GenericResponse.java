package org.uhc.ehr.data.service;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Schema(description = "Represens a REST API server response")
public class GenericResponse<T> {

    @Schema(description = "Contains a list of fetched records")
    List<T> data;


    @Schema(description = "Gets the total number of elements contained in the corresponding database table")
    long total;

    public GenericResponse() {

    }

    public GenericResponse(List<T> data) {
        this.data = data;
        if (data != null) {
            this.total = data.size();
        } else {
            this.total = 0;
        }
    }

}
