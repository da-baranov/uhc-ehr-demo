package org.uhc.ehr.data.service;

import org.uhc.ehr.data.dto.Patient;

public final class PatientResponse extends GenericResponse<Patient> {
}
