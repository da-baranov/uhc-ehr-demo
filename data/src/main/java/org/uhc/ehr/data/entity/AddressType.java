package org.uhc.ehr.data.entity;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "The type of an address (physical / postal)")
public enum AddressType {
    Postal,
    Physical,
    Both
}
