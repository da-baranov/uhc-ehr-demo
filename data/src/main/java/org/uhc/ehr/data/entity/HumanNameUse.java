package org.uhc.ehr.data.entity;

public enum HumanNameUse {
    Usual,
    Official,
    Temp,
    Nickname,
    Anonymous,
    Old,
    Maiden
}
