package org.uhc.ehr.data.entity;

public enum AddressUse {
    Home,
    Work,
    Temp,
    Old,
    Billing
}
