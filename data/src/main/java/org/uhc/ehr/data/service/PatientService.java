package org.uhc.ehr.data.service;

import lombok.NonNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uhc.ehr.data.Constants;
import org.uhc.ehr.data.DataException;
import org.uhc.ehr.data.NotFoundException;
import org.uhc.ehr.data.dto.ContactPoint;
import org.uhc.ehr.data.dto.HumanName;
import org.uhc.ehr.data.dto.Patient;
import org.uhc.ehr.data.dto.PersonAddress;
import org.uhc.ehr.data.entity.*;
import org.uhc.ehr.data.repository.*;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PatientService {

    final ModelMapper modelMapper;
    @Autowired
    PatientRepository patientRepository;
    @Autowired
    PersonNameRepository personNameRepository;
    @Autowired
    PersonAddressRepository personAddressRepository;
    @Autowired
    ContactPointRepository contactPointRepository;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    EntityManager entityManager;
    @Autowired
    CodeSystemRepository codeSystemRepository;

    public PatientService() {
        modelMapper = new ModelMapper();
    }

    public void deleteAll() {
        patientRepository.deleteAll();
    }

    public Patient map(PatientEntity value) {
        // Auto mapping
        var result = modelMapper.map(value, Patient.class);

        // Manual mapping - name
        var primaryName = value
                .getName()
                .stream()
                .filter(row -> HumanNameUse.Usual.equals(row.getUse()))
                .findFirst()
                .orElse(null);
        if (primaryName != null) {
            result.setFamilyName(primaryName.getFamily());
            if (primaryName.getGiven().size() > 0) {
                result.setFirstName(primaryName.getGiven().get(0));
            }
            if (primaryName.getGiven().size() > 1) {
                result.setLastName(primaryName.getGiven().get(1));
            }
        }

        // Manual mapping - gender
        if (value.getGender() != null) {
            result.setGender(value.getGender().getCode());
        }

        return result;
    }

    public PatientEntity map(Patient value) {
        // Auto mapping
        PatientEntity result;

        if (value.getUuid() == null) {
            result = modelMapper.map(value, PatientEntity.class);
        } else {
            result = patientRepository.findById(value.getUuid()).orElse(null);
            modelMapper.map(value, result);
        }

        // Manual mapping - name
        assert result != null;
        HumanNameEntity humanName = result
                .getName()
                .stream()
                .filter(row -> HumanNameUse.Usual.equals(row.getUse()))
                .findFirst()
                .orElse(null);

        if (humanName == null) {
            humanName = new HumanNameEntity();
            result.getName().add(humanName);
            humanName.setPerson(result);
        }
        humanName.setFamily(value.getFamilyName());

        humanName.getGiven().clear();
        if (value.getFirstName() != null) {
            humanName.getGiven().add(value.getFirstName());
        }
        if (value.getLastName() != null) {
            humanName.getGiven().add(value.getLastName());
        }

        // Manual mapping - gender
        if (value.getGender() != null) {
            var genderRow = codeSystemRepository
                    .findCoding(Constants.CODE_SYSTEM_GENDER, value.getGender());
            result.setGender(genderRow);
        } else {
            result.setGender(null);
        }

        return result;
    }

    public HumanName map(HumanNameEntity value) {
        var result = new HumanName();
        result.setUuid(value.getUuid());
        result.setUse(value.getUse());
        result.setFamilyName(value.getFamily());
        if (value.getGiven().size() > 0) {
            result.setFirstName(value.getGiven().get(0));
        } else {
            result.setFirstName(null);
        }
        if (value.getGiven().size() > 1) {
            result.setLastName(value.getGiven().get(1));
        } else {
            result.setLastName(null);
        }
        if (value.getPrefix().size() > 0) {
            result.setPrefix(value.getPrefix().get(0));
        } else {
            result.setPrefix(null);
        }
        if (value.getSuffix().size() > 0) {
            result.setSuffix(value.getSuffix().get(0));
        } else {
            result.setSuffix(null);
        }
        return result;
    }

    public void map(HumanName value, /* out */ HumanNameEntity result) {
        if (result == null) result = new HumanNameEntity();
        result.setUuid(value.getUuid());
        result.setUse(value.getUse());
        result.setFamily(value.getFamilyName());
        result.getGiven().clear();
        result.getGiven().add(value.getFirstName() == null ? "" : value.getFirstName());
        if (value.getLastName() != null) result.getGiven().add(value.getLastName());
        result.getPrefix().clear();
        if (value.getPrefix() != null) {
            result.getPrefix().add(value.getPrefix());
        }
        result.getSuffix().clear();
        if (value.getSuffix() != null) {
            result.getSuffix().add(value.getSuffix());
        }
    }

    public HumanNameEntity map(HumanName value) {
        var result = new HumanNameEntity();
        map(value, result);
        return result;
    }

    public ContactPoint map(ContactPointEntity value) {
        return modelMapper.map(value, ContactPoint.class);
    }

    public ContactPointEntity map(ContactPoint value) {
        return modelMapper.map(value, ContactPointEntity.class);
    }

    public void setPatientPhoto(UUID patientId, byte[] value) {
        var patientEntity = loadPatientEntity(patientId);
        patientEntity.setPhoto(value);
        patientRepository.save(patientEntity);
    }

    public void deletePatientPhoto(UUID patientId) {
        var patientEntity = loadPatientEntity(patientId);
        patientEntity.setPhoto(null);
        patientRepository.save(patientEntity);
    }

    public Patient insertPatient(Patient value) {
        Objects.requireNonNull(value);

        var entity = map(value);
        // entity.setAuthor(SecurityContextHolder.getContext().getAuthentication())
        entity.setUuid(null);
        patientRepository.save(entity);

        return loadPatient(entity.getUuid());
    }

    public ContactPoint insertPatientContact(@NonNull UUID patientId, @NonNull ContactPoint value) {
        var patientEntity = loadPatientEntity(patientId);
        var contactPointEntity = map(value);
        contactPointEntity.setPerson(patientEntity);
        contactPointRepository.save(contactPointEntity);
        return getPatientContact(contactPointEntity.getUuid());
    }

    public HumanName insertPatientName(@NonNull UUID patientId, @NonNull HumanName value) {
        var patientEntity = loadPatientEntity(patientId);
        var humanNameEntity = map(value);
        humanNameEntity.setPerson(patientEntity);
        personNameRepository.save(humanNameEntity);
        return getPatientName(humanNameEntity.getUuid());
    }

    public PersonAddress map(PersonAddressEntity value) {
        var result = modelMapper.map(value, PersonAddress.class);
        if (value.getLine().size() > 0)
            result.setLine(value.getLine().get(0));
        return result;
    }

    public PersonAddressEntity map(PersonAddress value) {
        var result = modelMapper.map(value, PersonAddressEntity.class);
        result.getLine().clear();
        if (value.getLine() != null) {
            result.getLine().add(value.getLine());
        }
        return result;
    }

    public Patient loadPatient(@NonNull UUID uuid) {
        var entity = patientRepository.findById(uuid).orElse(null);
        if (entity == null) {
            throw new DataException("Person not found");
        }
        return map(entity);
    }

    public byte[] loadPatientPhoto(@NonNull UUID uuid) {
        var patientEntity = loadPatientEntity(uuid);
        return patientEntity.getPhoto();
    }

    public Patient updatePatient(@NonNull UUID patientId, @NonNull Patient value) {

        Objects.requireNonNull(value);

        var entity = map(value);

        patientRepository.save(entity);

        return loadPatient(entity.getUuid());
    }

    private Calendar toCalendar(LocalDateTime value) {
        if (value == null) return null;

        var calendar = Calendar.getInstance();
        calendar.clear();
        calendar.set(value.getYear(), value.getMonthValue() - 1, value.getDayOfMonth(),
                value.getHour(), value.getMinute(), value.getSecond());
        return calendar;
    }

    public PatientResponse getPatients(PatientRequest request) {
        if (request == null) {
            request = new PatientRequest();
        }

        var result = new PatientResponse();

        var parameters = new HashMap<String, Object>();
        var baseSql =
                "select p from PatientEntity p " +
                        "  left join p.name h " +
                        "    left join h.given hg " +
                        " where 1 = 1 ";
        if (request.id != null && !request.id.isBlank()) {
            request.id = request.id.replace('*', '%');
            baseSql += " and upper(p.id) like upper(:id)";
            parameters.put("id", request.id);
        }

        if (request.familyName != null && !request.familyName.isBlank()) {
            request.familyName = request.familyName.replace('*', '%');
            baseSql += " and upper(h.family) like upper(:family) ";
            parameters.put("family", request.familyName);
        }

        if (request.firstName != null && !request.firstName.isBlank()) {
            request.firstName = request.firstName.replace('*', '%');
            baseSql += " and upper(hg) like upper(:given) ";
            parameters.put("given", request.firstName);
        }

        if (request.birthDateFrom != null) {
            baseSql += " and p.birthDate >= :bdf";
            parameters.put("bdf", toCalendar(request.birthDateFrom));
        }

        if (request.birthDateTo != null) {
            baseSql += " and p.birthDate <= :bdt";
            parameters.put("pdt", toCalendar(request.birthDateTo));
        }

        var sql = baseSql;
        if (request.get_sort() != null) {
            if ("id".equalsIgnoreCase(request.get_sort())) {
                sql += " order by p.id";
            }
            else if ("familyName".equalsIgnoreCase(request.get_sort())) {
                sql += " order by h.family ";
            }
            else if ("firstName".equalsIgnoreCase(request.get_sort())) {
                sql += " order by hg";
            }
            else if ("gender".equals(request.get_sort())) {
                sql += " order by p.gender";
            }
            else if ("birthDate".equals(request.get_sort())) {
                sql += " order by p.birthDate";
            }
        }

        // Executing query
        var query = entityManager.createQuery(sql, PatientEntity.class);
        for (var p : parameters.entrySet()) {
            query.setParameter(p.getKey(), p.getValue());
        }
        query.setFirstResult(request.get_offset());
        query.setMaxResults(request.get_count());
        var list =
                query
                        .getResultList()
                        .stream()
                        .map(this::map)
                        .collect(Collectors.toList());
        result.setData(list);

        // Counting total records
        var countSql = baseSql.replace("select p", "select count(distinct p.uuid)");
        var countQuery = entityManager.createQuery(countSql, Long.class);
        for (var p : parameters.entrySet()) {
            countQuery.setParameter(p.getKey(), p.getValue());
        }
        var cnt = countQuery.getSingleResult();
        result.setTotal(cnt);

        // Returning data
        return result;
    }

    public List<HumanName> getPatientNames(@NonNull UUID patientId) {
        var patientEntity = loadPatientEntity(patientId);
        var list = patientEntity
                .getName()
                .stream()
                .map(this::map)
                .collect(Collectors.toList());
        return list;
    }

    public HumanName getPatientName(@NonNull UUID patientId, @NonNull UUID nameId) {
        var patientEntity = patientRepository.findById(patientId).orElse(null);
        if (patientEntity == null) {
            throw new NotFoundException(Patient.class, patientId);
        }
        var nameEntity = patientEntity
                .getName()
                .stream()
                .filter(row -> nameId.equals(row.getUuid()))
                .findFirst()
                .orElse(null);
        if (nameEntity == null) throw new NotFoundException(HumanNameEntity.class, nameId);
        return map(nameEntity);
    }

    public HumanName getPatientName(@NonNull UUID patientNameId) {
        var humanNameEntity = personNameRepository.findById(patientNameId).orElse(null);
        if (humanNameEntity == null) throw new NotFoundException(HumanName.class, patientNameId);
        return map(humanNameEntity);
    }

    public List<ContactPoint> getPatientContacts(@NonNull UUID patientId) {
        var patientEntity = loadPatientEntity(patientId);

        return patientEntity
                .getContact()
                .stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    public ContactPoint getPatientContact(@NonNull UUID contactPointId) {
        var contactPointEntity = contactPointRepository.findById(contactPointId).orElse(null);
        if (contactPointEntity == null) {
            throw new NotFoundException(ContactPoint.class, contactPointId);
        }
        return map(contactPointEntity);
    }

    public List<PersonAddress> getPatientAddresses(@NonNull UUID patientId) {
        var patientEntity = loadPatientEntity(patientId);
        return patientEntity
                .getAddress()
                .stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    public PersonAddress getPatientAddress(@NonNull UUID addressId) {
        var addressEntity = this.personAddressRepository.findById(addressId).orElse(null);
        if (addressEntity == null) {
            throw new NotFoundException(PersonAddress.class, addressId);
        }
        return map(addressEntity);
    }

    private PatientEntity loadPatientEntity(@NonNull UUID patientId) {
        var result = patientRepository.findById(patientId).orElse(null);
        if (result == null) {
            throw new NotFoundException(PatientEntity.class, patientId);
        }
        return result;
    }

    public void deletePatient(@NonNull UUID patientId) {
        var patientEntity = loadPatientEntity(patientId);
        patientRepository.deleteById(patientId);
    }

    @Transactional
    public void deletePatientName(
            @NonNull UUID patientId,
            @NonNull UUID nameId) {
        var personEntity = loadPatientEntity(patientId);
        var names = personEntity.getName();
        if (names.size() == 1) {
            throw new DataException("You cannot remove the only patient name.");
        }

        var personName =
                names
                .stream()
                .filter(row -> nameId.equals(row.getUuid()))
                .findFirst()
                .orElse(null);
        if (personName == null) {
            throw new NotFoundException(HumanNameEntity.class, nameId);
        }
        personEntity.getName().remove(personName);
        personName.setPerson(null);
        personNameRepository.delete(personName);
    }

    public void deletePatientAddress(
            @NonNull UUID patientId,
            @NonNull UUID personAddressId) {
        personAddressRepository.deleteById(personAddressId);
    }

    public void deletePatientContact(
            @NonNull UUID patientId,
            @NonNull UUID contactPointId
    ) {
        contactPointRepository.deleteById(contactPointId);
    }

    public PersonAddress insertPatientAddress(@NonNull UUID patientId, @NonNull PersonAddress address) {
        var patientEntity = loadPatientEntity(patientId);
        var addressEntity = map(address);
        addressEntity.setPerson(patientEntity);
        personAddressRepository.save(addressEntity);
        return getPatientAddress(addressEntity.getUuid());
    }

    public HumanName updatePatientName(@NonNull UUID patientId,
                                       @NonNull UUID nameId,
                                       @NonNull HumanName humanName) {
        var patientEntity = loadPatientEntity(patientId);
        var humanNameEntity = patientEntity
                .getName()
                .stream()
                .filter(row -> nameId.equals(row.getUuid()))
                .findFirst()
                .orElse(null);
        if (humanNameEntity == null) {
            throw new NotFoundException(HumanNameEntity.class, nameId);
        }
        map(humanName, humanNameEntity);
        patientRepository.save(patientEntity);
        return this.getPatientName(humanNameEntity.getUuid());
    }

    public PersonAddress updatePatientAddress(
            @NonNull UUID patientId,
            @NonNull UUID addressId,
            @NonNull PersonAddress personAddress) {
        var patientEntity = loadPatientEntity(patientId);
        var addressEntity = personAddressRepository.findById(personAddress.getUuid()).orElse(null);
        if (personAddress == null) {
            throw new NotFoundException(PersonAddress.class, personAddress.getUuid());
        }
        this.personAddressRepository.save(addressEntity);
        return getPatientAddress(personAddress.getUuid());
    }

    public ContactPoint updatePatientContact(
            @NonNull UUID patientId,
            @NonNull UUID contactId,
            @NonNull ContactPoint contactPoint) {
        var patientEntity = loadPatientEntity(patientId);
        var contactPointEntity = this.contactPointRepository.findById(contactPoint.getUuid()).orElse(null);
        if (contactPointEntity == null) {
            throw new NotFoundException(ContactPoint.class, contactPoint.getUuid());
        }
        contactPointRepository.save(contactPointEntity);
        return getPatientContact(contactPointEntity.getUuid());
    }
}
