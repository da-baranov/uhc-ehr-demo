package org.uhc.ehr.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.*;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "HumanNames")
public class HumanNameEntity {
    @Id
    @Column
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    UUID uuid;

    @Enumerated(EnumType.STRING)
    @NotNull(message = "Name use is required")
    HumanNameUse use = HumanNameUse.Usual;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(nullable = false, name = "PersonUuid")
    PersonEntity person;

    @Audited
    String text;

    @Column(nullable = false)
    @Audited
    @NotNull(message = "Family name is required")
    @NotBlank(message = "Family name is required")
    String family;

    @ElementCollection
    @CollectionTable(name = "HumanNamesGiven", joinColumns = {@JoinColumn(name = "HumanNameUuid")})
    @JoinColumn(name = "HumanNameUuid")            // name of the @Id column of this entity
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Cascade(CascadeType.ALL)
    @OrderColumn(name = "position")
    @Audited
    List<String> given = new ArrayList<>();

    @ElementCollection
    @CollectionTable(name = "HumanNamesPrefix", joinColumns = {@JoinColumn(name = "HumanNameUuid")})
    @JoinColumn(name = "HumanNameUuid")            // name of the @Id column of this entity
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Cascade(CascadeType.ALL)
    @OrderColumn(name = "position")
    @Audited
    List<String> prefix = new ArrayList<>();

    @ElementCollection
    @CollectionTable(name = "HumanNamesSuffix", joinColumns = {@JoinColumn(name = "HumanNameUuid")})
    @JoinColumn(name = "HumanNameUuid")            // name of the @Id column of this entity
    @OnDelete(action = OnDeleteAction.CASCADE)
    @Cascade(CascadeType.ALL)
    @OrderColumn(name = "position")
    @Audited
    List<String> suffix = new ArrayList<>();
}
