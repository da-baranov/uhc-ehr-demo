package org.uhc.ehr.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.UUID;

@Table(name = "Codings", uniqueConstraints = {
        @UniqueConstraint(name = "c_coding_code_uk", columnNames = {"CodeSystemVersionUuid", "Code"})
})
@Entity
@Getter
@Setter
public class CodingEntity {
    @Id
    @Column
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    UUID uuid;

    @ManyToOne
    @JoinColumn(nullable = false, name = "CodeSystemVersionUuid")
    @JsonIgnore
    CodeSystemVersionEntity codeSystemVersion;

    @Column(nullable = false)
    String code;

    @Column(nullable = false)
    String display;
}
