package org.uhc.ehr.data.service;

import org.uhc.ehr.data.dto.PersonAddress;

public class PatientAddressesResponse extends GenericResponse<PersonAddress> {
}
