package org.uhc.ehr.data.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.springframework.context.ApplicationContext;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Table(name = "Persons", uniqueConstraints = {
        @UniqueConstraint(name = "c_person_id_uk", columnNames = {"id"})
})
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Audited
public class PersonEntity implements Serializable {

    @Id
    @Column
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Type(type = "uuid-char")
    UUID uuid;

    @Column(nullable = false)
    @Audited
    @NotNull(message = "Person identifier is required")
    @NotBlank(message = "Person identifier is required")
    String id;

    @Audited
    Boolean active = Boolean.TRUE;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "person", orphanRemoval = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotAudited
    Set<HumanNameEntity> name = new HashSet<>();

    @Column
    @Audited
    Calendar birthDate;

    @Column
    @Audited
    Calendar deceased;

    @Column
    Calendar createDate;

    @Column
    Calendar modifyDate;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "person", fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotAudited
    Set<PersonAddressEntity> address = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "person", fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @NotAudited
    Set<ContactPointEntity> contact = new HashSet<>();

    @ManyToOne
    @Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
    CodingEntity gender;

    @Basic(fetch = FetchType.LAZY)
    @Lob
    @Column(length = 1024 * 1024 * 10)
    byte[] photo;

    @PrePersist
    void onCreate() {
        createDate = Calendar.getInstance();
    }

    @PreUpdate()
    void onUpdate() {
        modifyDate = Calendar.getInstance();
    }

    // @Version
    // private Integer version;
}
