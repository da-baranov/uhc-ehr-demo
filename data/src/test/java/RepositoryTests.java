import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;
import org.uhc.ehr.data.TestApplication;
import org.uhc.ehr.data.dto.Patient;
import org.uhc.ehr.data.service.PatientRequest;
import org.uhc.ehr.data.service.PatientService;
import org.uhc.ehr.data.service.SeedingService;
import java.util.Calendar;
import java.util.UUID;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = TestApplication.class)
public class RepositoryTests {

    @Autowired
    PatientService patientService;
    @Autowired
    SeedingService seedingService;

    @BeforeEach
    @Transactional
    @Rollback(value = false)
    public void seed() {
        seedingService.seed();
    }

    @Test
    @Order(1)
    @Transactional
    @Rollback(value = false)
    public void insertPatientTest() {
        var patient = new Patient();
        patient.setId(UUID.randomUUID().toString());
        patient.setFamilyName("Baranov");
        patient.setFirstName("Dmitry");
        patient.setLastName("A");
        patient.setBirthDate(Calendar.getInstance());
        patient.setGender("male");
        patient.setActive(true);
        patientService.insertPatient(patient);
    }

    @Test
    @Order(2)
    @Transactional
    @Rollback(value = false)
    public void updatePatientTest() {
        var request = new PatientRequest();
        request.set_count(10);
        for (var patient : patientService.getPatients(request).getData()) {
            patient.setActive(!patient.getActive());
            patient.setBirthDate(Calendar.getInstance());
            patientService.updatePatient(patient.getUuid(), patient);
        }
        assertTrue(true);
    }

    @Test
    @Order(3)
    @Transactional
    @Rollback(value = false)
    public void findByNameTest() {
        var request = new PatientRequest();
        request.setFamilyName("*a*");
        var result = patientService.getPatients(request);
        assertTrue(result.getData() != null && result.getData().size() > 0);
    }
}
