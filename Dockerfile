FROM openjdk:11
ARG JAR_FILE=client/target/*.jar
COPY ${JAR_FILE} uhc-client.jar
ENTRYPOINT ["java", "-jar", "/uhc-client.jar"]
