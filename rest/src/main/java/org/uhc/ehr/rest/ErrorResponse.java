package org.uhc.ehr.rest;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@Schema(description = "Generic REST API error")
public class ErrorResponse {

    // For ExtJS
    @Schema(description = "Operation success status")
    @NotNull
    boolean success;

    @Schema(description = "Gets a message that describes the current API error.")
    @NotNull
    String message;

    public ErrorResponse() {
    }

    public ErrorResponse(Throwable e) {
        StringBuilder sb = new StringBuilder();
        while (e != null) {
            sb.append(e.getMessage());
            sb.append("\r\n");
            e = e.getCause();
        }
        this.setMessage(sb.toString());
        this.setSuccess(false);
    }
}
