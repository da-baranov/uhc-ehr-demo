package org.uhc.ehr.rest;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;

@OpenAPIDefinition(
        info = @Info(
                version = "0.0.1",
                title = "University Hospital Cologne REST API",
                description = "Patient Management API",
                contact = @Contact(
                        name = "Dmitry Baranov",
                        email = "d.a.baranov@gmail.com"
                )
        )
)
@SecurityScheme(
        name = "api",
        scheme = "basic",
        type = SecuritySchemeType.APIKEY,
        in = SecuritySchemeIn.HEADER)
public final class OpenAPIConfiguration {
}
