package org.uhc.ehr.rest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuccessResponse {
    boolean success = true;
}
