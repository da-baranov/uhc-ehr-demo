package org.uhc.ehr.rest;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.uhc.ehr.data.DataConfiguration;

@Configuration
@Import(value = DataConfiguration.class)
public class RestConfiguration {
}
