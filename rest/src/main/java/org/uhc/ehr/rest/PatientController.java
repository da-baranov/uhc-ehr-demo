package org.uhc.ehr.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.uhc.ehr.data.NotFoundException;
import org.uhc.ehr.data.dto.ContactPoint;
import org.uhc.ehr.data.dto.HumanName;
import org.uhc.ehr.data.dto.Patient;
import org.uhc.ehr.data.dto.PersonAddress;
import org.uhc.ehr.data.service.*;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

@RestController()
@RequestMapping("/api/v1/patient")
@Description("Patient management service")
@Slf4j(topic = "PatientController")
@Tag(name = "Patient management API", description = "Provides patient management REST API functions")
public class PatientController {

    final PatientService patientService;
    final SeedingService seedingService;

    public PatientController(SeedingService seedingService, PatientService patientService) {
        this.seedingService = seedingService;
        this.patientService = patientService;
    }

    @InitBinder
    public void customizeBinding(WebDataBinder binder) {
        binder.setFieldMarkerPrefix(null);
    }

    private ResponseEntity<SuccessResponse> success() {
        return ResponseEntity.ok(new SuccessResponse());
    }

    private <T> ResponseEntity<T> created(String uri, T body) {
        try {
            var u = new URI(uri);
            return ResponseEntity
                    .created(u)
                    .body(body);
        } catch (URISyntaxException uex) {
            throw new RuntimeException(uex);
        }
    }

    @Operation(description = "Seed some test data")
    @PostMapping("/seed")
    public ResponseEntity<SuccessResponse> seed() {
        seedingService.seed();
        return success();
    }

    @Operation(description = "Deletes all patients")
    @PostMapping("/clear")
    public ResponseEntity<SuccessResponse> clear() {
        seedingService.deleteAllPatients();
        return success();
    }

    @Operation(description = "Loads a list of patient names")
    @GetMapping("/{patientId}/name")
    public ResponseEntity<PatientNamesResponse> getPatientNames(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId) {
        var names = patientService.getPatientNames(patientId);
        var response = new PatientNamesResponse();
        response.setData(names);
        return ResponseEntity.ok(response);
    }

    @Operation(description = "Loads a single patient name record")
    @GetMapping("/{patientId}/name/{nameId}")
    public ResponseEntity<HumanName> getPatientName(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
            UUID patientId,
            @Parameter(description = "Patient name identifier", required = true)
            @PathVariable
            UUID nameId) {
        var name = patientService.getPatientName(patientId, nameId);
        return ResponseEntity.ok(name);
    }

    @Operation(description = "Load the patient from the database")
    @GetMapping("/{patientId}")
    public ResponseEntity<Patient> loadPatient(
            @PathVariable
            @Parameter(description = "Patient identifier", required = true)
            UUID patientId
    ) {
        var patient = patientService.loadPatient(patientId);
        return ResponseEntity.ok(patient);
    }

    @Operation(description = "Loads a list of patients matching a complex set of user-defined search criteria")
    @GetMapping("")
    @ApiResponse(responseCode = "200", description = "List of patients", content = @Content(schema = @Schema(implementation = PatientResponse.class)))
    public ResponseEntity<PatientResponse> getPatients(
            @Parameter(description = "User-defined search criteria, such as patient name, gender and so on")
            @ParameterObject
                    PatientRequest patientSearchParams
    ) {
        var result = patientService.getPatients(patientSearchParams);
        return ResponseEntity.ok(result);
    }

    @Operation(description = "Register a new patient record in the database")
    @PostMapping(value = "")
    @ApiResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = Patient.class)))
    public ResponseEntity<Patient> insertPatient(
            @Parameter(description = "Patient record to be inserted", required = true)
            @RequestBody
            @Valid
                    Patient value
    ) {
        var result = patientService.insertPatient(value);
        return this.created("/api/v1/patient/" + result.getUuid(), result);
    }

    @Operation(description = "Updates the patient record in the database")
    @PutMapping(value = "/{patientId}")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = Patient.class)))
    public ResponseEntity<Patient> updatePatient(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
            UUID patientId,

            @Parameter(description = "Patient record to be updated", required = true)
            @RequestBody
            @Valid
            Patient value
    ) {
            var result = patientService.updatePatient(patientId, value);
            return ResponseEntity.ok(result);
    }

    @Operation(description = "Delete the patient record")
    @DeleteMapping("/{patientId}")
    public ResponseEntity<SuccessResponse> deletePatient(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
            UUID patientId
    ) {
        patientService.deletePatient(patientId);
        return success();
    }

    @Operation(description = "Registers a new patient name")
    @PostMapping(value = "/{patientId}/name")
    @ApiResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = HumanName.class)))
    public ResponseEntity<HumanName> insertPatientName(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId,

            @Parameter(description = "Patient name", required = true)
            @RequestBody
            @Valid
                    HumanName value) {
        var name = patientService.insertPatientName(patientId, value);
        return created("/api/v1/patient/" + patientId + "/name/" + name.getUuid(), name);
    }

    @Operation(description = "Updates an existing patient name")
    @PutMapping(value = "/{patientId}/name/{nameId}")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = HumanName.class)))
    public ResponseEntity<HumanName> updatePatientName(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId,

            @Parameter(description = "Patient name identifier", required = true)
            @PathVariable
                    UUID nameId,

            @Parameter(description = "Patient name", required = true)
            @RequestBody
            @Valid
                    HumanName value) {
        var name = patientService.updatePatientName(patientId, nameId, value);
        return ResponseEntity.ok(name);
    }

    @Operation(description = "Deletes the patient name")
    @DeleteMapping("/{patientId}/name/{nameId}")
    public ResponseEntity<SuccessResponse> deletePatientName(
            @Parameter(description = "Patient identifier")
            @PathVariable
                    UUID patientId,

            @Parameter(description = "Patient name identifier")
            @PathVariable
                    UUID nameId) {
        patientService.deletePatientName(patientId, nameId);
        return success();
    }

    @Operation(description = "Registers a new patient address")
    @PostMapping(value = "/{patientId}/address")
    @ApiResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = PersonAddress.class)))
    public ResponseEntity<PersonAddress> insertPatientAddress(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId,

            @Parameter(description = "Patient address", required = true)
            @Valid
            @RequestBody
                    PersonAddress value) {
        var address = patientService.insertPatientAddress(patientId, value);
        return created("/api/v1/patient/" + patientId + "/address/" + address.getUuid(), address);
    }

    @Operation(description = "Updates the patient address")
    @PutMapping(value = "/{patientId}/address/{addressId}")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = PersonAddress.class)))
    public ResponseEntity<PersonAddress> updatePatientAddress(
            @Parameter(description = "Patient identifier")
            @PathVariable
                    UUID patientId,

            @Parameter(description = "Address identifier")
            @PathVariable
                    UUID addressId,

            @Parameter(description = "Patient address")
            @Valid
            @RequestBody
                    PersonAddress value) {
        var address = patientService.updatePatientAddress(patientId, addressId, value);
        return ResponseEntity.ok(address);
    }

    @Operation(description = "Loads a list of patient addresses")
    @GetMapping("/{patientId}/address")
    public ResponseEntity<PatientAddressesResponse> getPatientAddresses(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
            UUID patientId
    ) {
        var addresses = patientService.getPatientAddresses(patientId);
        var response = new PatientAddressesResponse();
        response.setData(addresses);
        return ResponseEntity.ok(response);
    }

    @Operation(description = "Deletes the patient address")
    @DeleteMapping("/{patientId}/address/{addressId}")
    public ResponseEntity<SuccessResponse> deletePatientAddress(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId,

            @Parameter(description = "Patient address identifier", required = true)
            @PathVariable
                    UUID addressId) {
        patientService.deletePatientAddress(patientId, addressId);
        return success();
    }

    @Operation(description = "Registers a patient contact record")
    @PostMapping(value = "/{patientId}/contact")
    @ApiResponse(responseCode = "201", content = @Content(schema = @Schema(implementation = ContactPoint.class)))
    public ResponseEntity<ContactPoint> insertPatientContact(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId,

            @Parameter(description = "A contact detail for the individual", required = true)
            @Valid
                    ContactPoint value) {

        var contactPoint = patientService.insertPatientContact(patientId, value);
        return created("/api/v1/patient/" + patientId + "/contact/" + contactPoint.getUuid(), contactPoint);
    }

    @Operation(description = "Updates a patient contact record")
    @PutMapping(value = "/{patientId}/contact/{contactId}")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = ContactPoint.class)))
    public ResponseEntity<ContactPoint> updatePatientContact(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId,

            @Parameter(description = "Contact identifier", required = true)
            @PathVariable
                    UUID contactId,

            @Parameter(description = "A contact detail for the individual", required = true)
            @Valid
                    ContactPoint value) {

        var contactPoint = patientService.updatePatientContact(patientId, contactId, value);
        return ResponseEntity.ok(contactPoint);
    }

    @Operation(description = "Removes a contact record")
    @DeleteMapping("/{patientId}/contact/{contactId}")
    public ResponseEntity<SuccessResponse> deletePatientContact(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId,

            @Parameter(description = "Patient contact identifier", required = true)
            @PathVariable
                    UUID contactId) {
        patientService.deletePatientContact(patientId, contactId);
        return success();
    }

    @Operation(description = "Loads a list of patient contacts")
    @GetMapping("/{patientId}/contact")
    public ResponseEntity<PatientContactsResponse> getPatientContacts(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId
    ) {
        var contacts = patientService.getPatientContacts(patientId);
        var response = new PatientContactsResponse();
        response.setData(contacts);
        return ResponseEntity.ok(response);
    }

    @Operation(description = "Loads a picture of the patient")
    @GetMapping(value = "/{patientId}/photo", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public ResponseEntity<byte[]> getPatientPhoto(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId
    )
    throws IOException
    {
        var bytes = patientService.loadPatientPhoto(patientId);
        if (bytes == null || bytes.length == 0) {
            bytes = this.getClass().getClassLoader().getResourceAsStream("public/empty.png").readAllBytes();
        }
        return ResponseEntity.ok(bytes);
    }

    @Operation(description = "Stores a patient photo in the database")
    @RequestMapping(
            value = "/{patientId}/photo",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            method = {RequestMethod.POST})
    public ResponseEntity<SuccessResponse> savePatientPhoto(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId,

            @Parameter(description = "A picture (supported formats are JPG, PNG, and BMP)", required = true)
            @NonNull
            @RequestPart("file")
            MultipartFile file)
            throws IOException
    {
        patientService.setPatientPhoto(patientId, file.getBytes());
        return success();
    }

    @Operation(description = "Deletes the patient photo from the database")
    @DeleteMapping("/{patientId}/photo")
    public ResponseEntity<SuccessResponse> deletePatientPhoto(
            @Parameter(description = "Patient identifier", required = true)
            @PathVariable
                    UUID patientId
    ) {
        patientService.deletePatientPhoto(patientId);
        return success();
    }

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ErrorResponse> handleNotFound(NotFoundException exception) {
        log.error("Record not found", exception);

        var error = new ErrorResponse(exception);
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(error);
    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ErrorResponse> handleThrowable(Throwable exception) {
        log.error("Unspecified error", exception);

        var error = new ErrorResponse(exception);
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(error);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handlevalidationErrors(MethodArgumentNotValidException exception) {
        log.error("Validation error", exception);
        var errorMessage = "These are some errors, please correct them below: \r\n";
        for (var error : exception.getAllErrors()) {
            errorMessage += error.toString() + "\r\n";
        }
        var error = new ErrorResponse(exception);
        error.setMessage(errorMessage);
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .body(error);
    }
}
