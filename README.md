# University Hospital Cologne EHR Demo

## Used frameworks

**Backend**

- Java 11 (Microsoft)
- Spring Boot
- Apache Maven
- JPA
- Hibernate
- H2 Database engine

**Frontend**

- Sencha ExtJS 7.0 GPL

**CI/CD**

- GitLab
- Docker

## Common architecture

The main project is a Maven BOM application which includes three modules:
- `data` (provides access to an application database)
- `rest` (provides pluggable JSON web services)
- `client` (obviously, a client single-page application)

## Data access layer

H2 database is a good choice for fast prototyping. 
An instance of in-memory H2 database is used by automated tests, 
and another instance of file H2 database is used in production.

Java JPA technology provides an object-relational mapping (ORM) between the H2 database 
and service layer. Hibernate is being used as a JPA implementation library.

ORM entities have the following specific features:
- UUIDs are used for primary keys, which is pretty handy for data replication
- Auditing capabilities are implemented with the Envers library
- Entities are conformant to HL7 FHIR resources (e.g. Patient).

Low-level JPA repositories are backed by more specific service classes that
implement main application business logic.

## Service layer

Web services are implemented as typical Spring Boot REST services with minimal 
improvements. Each REST service provides the following typical operations:
- Data search 
- Create an object
- Update an object
- Delete an object

REST API is backed by Swagger that provides a consumer with a comprehensive API description.

## Client (frontend) application

Implemented as a single-page [Sencha ExtJS](https://sencha.com) application.

## Security

HTTP security was not implemented for simplicity, but there is a Swagger stub 
which notifies a REST service consumer that API KEY should be used for authentication and authorization. 

## Testing

There are two types of automated tests within the project - unit tests and integration tests.

Unit tests are used mostly to cover the functionality of JPA data access repositories and services,
while integration tests are used to test application REST services and for checking HTTP response codes.

## REST exception hangling

Provided by the Spring `@ExceptionHandler` annotation. Most common exceptions are get wrapped 
to a handy `ErrorResponse` structure that returns a consumer specific HTTP error codes, operation statuses, and exception messages.

## Logging

Implemented with `Log4j`. 

## CI/CD

The project source code is hosted on `GitLab` which is not just the code repository 
but the powerful integration and delivery service. As a result of CI/CD build-test-deploy pipeline 
we get a docker container which can be deployed to a production server.
Alternaltively, the docker image can be deployed on Docker Hub.











