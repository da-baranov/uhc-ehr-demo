package org.uhc.ehr.rest;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A very simple REST API protection with an API key
 */
public class ApiTokenFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;

        // TODO: check the Authorization header value, get API key, validate the key, return 401 if invalid
        var authHeaderValue = req.getHeader("Authorization");
        if (authHeaderValue == null || authHeaderValue.isBlank()) {
            ((HttpServletResponse) servletResponse).setStatus(401);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
