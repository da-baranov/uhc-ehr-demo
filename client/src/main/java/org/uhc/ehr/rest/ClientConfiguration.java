package org.uhc.ehr.rest;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.uhc.ehr.data.DataConfiguration;

@Configuration
@Import(value = {DataConfiguration.class, RestConfiguration.class})
public class ClientConfiguration {

    @Bean
    public FilterRegistrationBean<ApiTokenFilter> apiTokenFilter(){
        var filterBean = new FilterRegistrationBean<ApiTokenFilter>();
        filterBean.setFilter(new ApiTokenFilter());
        filterBean.addUrlPatterns("/api/v1/patient/*");
        filterBean.setOrder(2);
        return filterBean;
    }
}
