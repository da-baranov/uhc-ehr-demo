Ext.application({
    extend: 'Uhc.Application',

    name: 'Uhc',

    requires: [
        'Uhc.*'
    ],

    mainView: 'Uhc.view.main.Main',

    launch: function () {
        const el = document.getElementById("loading-overlay");
        if (el) el.remove();
    }
});
