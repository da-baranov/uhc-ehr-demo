Ext.define("Uhc.store.Patient", {
    requires: [
        "Uhc.model.Patient",
        "Uhc.Constants"
    ],
    extend: "Ext.data.Store",
    model: "Uhc.model.Patient",
    alias: "store.patient",
    autoLoad: true,
    remoteSort: true,
    proxy: {
        type: "ajax",
        url: Uhc.Constants.API_PATIENTS,
        reader: {
            type: "json",
            rootProperty: "data"
        },
        simpleSortMode: true,
        startParam: "_offset",
        limitParam: "_count",
        sortParam: "_sort"
    }
});