Ext.define("Uhc.Util", {
    requires: [
        "Uhc.ErrorMessageBox"
    ],
    statics: {
        toError: function (e) {
            // Undefined
            if (!e) {
                return new Error("Unexpected error");
            }

            // String
            if (typeof e === "string" || e instanceof String) {
                return new Error(e);
            }

            // Error object - see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error
            if (e instanceof Error) {
                return e;
            }

            // ErrorEvent object - see https://developer.mozilla.org/en-US/docs/Web/API/Element/error_event
            if (e instanceof ErrorEvent) {
                return e.error;
            }

            // XMLHTTPRequest.response? - see https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest/response
            if (e.responseText) {
                let json = null;
                try {
                    json = JSON.parse(e.responseText);
                }
                catch (jex) {
                    json = undefined;
                }
                if (json && json.message) {
                    return new Error(json.message);
                }
                return new Error(e.responseText);
            }

            // Ext.Error
            if (e instanceof Ext.Error) {
                return new Error(e.toString());
            }

            // Ext.form.action.Action
            if (e instanceof Ext.form.action.Action) {
                if (e.result && e.result.message) {
                    return new Error(e.result.message);
                }
            }

            // Ext.data.Batch
            if (e instanceof Ext.data.Batch) {
                const exceptions = e.getExceptions();
                if (exceptions.length !== 0) {
                    return this.extractError(exceptions[0]);
                }
            }

            // Ext.Operation
            if (e instanceof Ext.data.operation.Operation) {
                const error = e.getError();
                if (!error) {
                    return new Error(
                        "Unexpected error of type Operation (operation.error == null)"
                    );
                }
                if (error.response) {
                    const response = error.response;
                    if (response.responseJson) {
                        if (response.responseJson.message && response.responseJson.success === false) {
                            const result = new Error(response.responseJson.message);
                            return result;
                        } else {
                            const result = new Error(`Error: ${response.responseJson.status}, status text: ${response.responseJson.statusText}, error message: ${response.responseJson.message || response.responseJson.error}`);
                            result.stack = response.responseJson.stack;
                            return result;
                        }
                    }
                    if (response.responseText) {
                        return new Error(response.responseText);
                    }
                    if (response.statusText) {
                        return new Error(response.statusText + " " + response.status);
                    }
                    if (response.status) {
                        return new Error("HTTP Error " + response.status);
                    }
                    return new Error(
                        "Unexpected error of type Operation (error.response.responseJson == null, error.response.responseText == null)"
                    );
                } else {
                    return new Error(
                        "Unexpected error of type Operation (error.response == null)"
                    );
                }
            }

            // Something else with a known property @message
            var err;
            if (e.message) {
                err = new Error(e.message);
                err.stack = e.stack || e.stackTrace;
                return err;
            }

            // Something else with a known property @msg
            if (e.msg) {
                err = new Error(e.msg);
                if (e.stack) err.stack = e.stack;
                return err;
            }

            console.log("Unknown error: " + e);
            return new Error("Unknown error: " + e);
        },

        raiseError: function (e) {
            const error = this.toError(e);
            if (error) throw error;
        },

        errorMessageBox: function (e, customMessage) {
            const error = this.toError(e);
            const message = customMessage ?
                customMessage + "\r\n" + error.message :
                error.message;
            const messageBox = new Uhc.ErrorMessageBox({ message: message, stack: error.stack });
            messageBox.show();
        },

        readFileAsString: function(encoding, filter) {
            encoding = encoding || "UTF-8";
            return new Promise(function(resolve, reject) {
                const fileEl = document.createElement("input");
                fileEl.type = "file";
                if (filter) fileEl.accept = filter;
                fileEl.style.display = "hidden";
                fileEl.onchange = function (e) {
                    const files = fileEl.files;
                    if (files.length === 0) {
                        resolve(undefined);
                        fileEl.remove();
                        return;
                    }

                    const reader = new FileReader();
                    reader.onerror = function(event) {
                        reject(event);
                        fileEl.remove();
                    };
                    reader.onload = function(event) {
                        resolve(event.target.result);
                        fileEl.remove();
                    };
                    reader.readAsText(files[0], encoding);
                }
                document.body.appendChild(fileEl);
                fileEl.click();
            });
        },

        loadPictureAsArray: function() {
            return new Promise(function(resolve, reject) {
                const fileEl = document.createElement("input");
                fileEl.type = "file";
                fileEl.accept = ".jpg,.png,.bmp";
                fileEl.style.display = "hidden";
                fileEl.onchange = function (e) {
                    const files = fileEl.files;
                    if (files.length === 0) {
                        resolve(undefined);
                        fileEl.remove();
                        return;
                    }

                    const reader = new FileReader();
                    reader.onerror = function(event) {
                        reject(event);
                        fileEl.remove();
                    };
                    reader.onload = function(event) {
                        resolve(event.target.result);
                        fileEl.remove();
                    };
                    reader.readAsArrayBuffer(files[0]);
                }
                document.body.appendChild(fileEl);
                fileEl.click();
            });
        },

        loadPictureAsFile: function() {
            return new Promise(function(resolve, reject) {
                const fileEl = document.createElement("input");
                fileEl.type = "file";
                fileEl.accept = ".jpg,.png,.bmp";
                fileEl.style.display = "hidden";
                fileEl.onchange = function (e) {
                    const files = fileEl.files;
                    if (files.length === 0) {
                        resolve(undefined);
                        fileEl.remove();
                        return;
                    } else {
                        resolve(files[0]);
                    }
                }
                document.body.appendChild(fileEl);
                fileEl.click();
            });
        }
    }
});