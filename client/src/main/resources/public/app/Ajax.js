Ext.define("Uhc.Ajax", {
    statics: {
        get: async function(url) {
            const response = await window.fetch(url);
            let json = {};
            json = await response.json();
            if (!response.ok) {
                throw json;
            }
            return json;
        },

        post: async function(url, data) {
            const response = await window.fetch(url, {
                method: "POST",
                body: data ? JSON.stringify(data) : undefined,
                headers: {
                    "Content-Type": "application/json"
                }
            });
            let json = {};
            json = await response.json();
            if (!response.ok) {
                throw json;
            }
            return json;
        },

        put: async function(url, data) {
            const response = await window.fetch(url, {
                method: "PUT",
                body: data ? JSON.stringify(data) : undefined,
                headers: {
                    "Content-Type": "application/json"
                }
            });
            let json = {};
            json = await response.json();
            if (!response.ok) {
                throw json;
            }
            return json;
        },

        delete: async function(url, data) {
            const response = await window.fetch(url, {
                method: "DELETE",
                body: data ? JSON.stringify(data) : undefined,
                headers: {
                    "Content-Type": "application/json"
                }
            });
            let json = {};
            json = await response.json();
            if (!response.ok) {
                throw json;
            }
            return json;
        }
    }
})