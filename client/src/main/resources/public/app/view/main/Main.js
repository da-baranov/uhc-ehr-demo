Ext.define('Uhc.view.main.Main', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'Uhc.view.main.MainController',
        'Uhc.view.main.MainModel',
        'Uhc.view.patients.Patients',
        'Uhc.view.NotImplemented'
    ],

    controller: 'main',
    viewModel: 'main',

    tbar: [
        {
            xtype: "image",
            src: Ext.getResourcePath("ukk-logo.svg"),
            height: 40,
            width: 100
        },
        {
            xtype: 'tbspacer',
            width: 20
        },
        {
            xtype: 'label',
            html: '<b>University Hospital Cologne</b> | EHR'
        },
        '->',
        {
            text: 'Swagger',
            iconCls: 'fa fa-code',
            handler: function () {
                window.open("/swagger-ui.html");
            }
        },
        {
            text: 'H2',
            iconCls: 'fa fa-database',
            handler: function () {
                window.open("/h2-console");
            }
        },
        {
            iconCls: 'fa fa-user',
            text: 'Login',
            hidden: true
        },
        {
            iconCls: 'fa fa-arrow-right',
            text: 'Logout',
            hidden: true
        }
    ],

    layout: "border",
    items: [
        {
            xtype: "treepanel",
            region: "west",
            width: "30%",
            title: "Menu",
            split: true,
            rootVisible: false,
            bind: {
                store: "{menu}",
                selection: "{selectedMenuItem}"
            }
        },
        {
            xtype: "container",
            region: "center",
            layout: "card",
            reference: "contentView",
            items: [
                {
                    xtype: "patients"
                },
                {
                    xtype: "ni"
                }
            ]
        }
    ]
});
