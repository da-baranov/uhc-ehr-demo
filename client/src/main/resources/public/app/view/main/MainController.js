Ext.define('Uhc.view.main.MainController', {
    requires: [
        "Uhc.view.Login"
    ],
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    bindings: {
        onMenuItemChange: '{selectedMenuItem}'
    },

    onMenuItemChange: function (selectedMenuItem) {
        const id = selectedMenuItem.get("id");
        const contentView = this.lookup("contentView");
        if (id === "patients") {
            contentView.setActiveItem(0);
        } else {
            contentView.setActiveItem(1);
        }
    },

    onLogin: function() {
        const form = Ext.create("Uhc.view.Login");
        form.show();
    }
});
