Ext.define('Uhc.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        selectedMenuItem: undefined
    },

    stores: {
        menu: {
            type: "tree",
            autoLoad: true,
            proxy: {
                type: "ajax",
                url: Ext.getResourcePath("menu.json"),
                reader: {
                    type: "json"
                }
            },
            root: {
                text: 'Menu',
                id: 'data',
                expanded: true
            }
        }
    }
});
