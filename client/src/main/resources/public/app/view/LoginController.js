Ext.define('Uhc.view.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',

    onFormShow: function() {
        this.getView().isValid();
    },

    onValidityChange: function(sender, valid) {
        this.getViewModel().set("formValid", valid);
        console.log("Form valid: " + valid);
    }
});
