Ext.define("Uhc.view.NotImplemented", {
    extend: "Ext.panel.Panel",
    alias: "widget.ni",
    title: "Coming soon",
    layout: "center",
    items: [
        {
            html: "This view has been not implemented yet"
        }
    ]
});