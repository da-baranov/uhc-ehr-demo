Ext.define('Uhc.view.patients.PatientForm', {
    extend: 'Ext.form.Panel',
    modal: true,
    floating: true,
    closable: true,
    baseCls: Ext.baseCSSPrefix + 'window',
    width: 600,
    height: 420,

    requires: [
        'Uhc.view.patients.PatientFormController',
        'Uhc.view.patients.PatientFormModel'
    ],

    controller: "patientform",
    viewModel: "patientform",

    layout: {
        type: "border"
    },
    bodyPadding: 4,
    modelValidation: true,
    defaultFocus: "#txtId",
    bind: {
        title: "Patient | {row.id} | {row.familyName}"
    },

    listeners: {
        validitychange: "onValidityChange",
        show: function(sender) {
            sender.isValid();
        }
    },

    keyMap: {
        ESC: {
            handler: function(e, view) {
                view.close();
            }
        }
    },

    items: [
        {
            xtype: "tabpanel",
            region: "center",
            items: [
                {
                    title: "Personal information",
                    layout: {
                        type: "vbox",
                        align: "stretch"
                    },
                    bodyPadding: 8,
                    items: [
                        {
                            xtype: "textfield",
                            fieldLabel: "Patient identifier",
                            itemId: "txtId",
                            msgTarget: "side",
                            bind: {
                                value: "{row.id}"
                            }
                        },
                        {
                            xtype: "textfield",
                            fieldLabel: "Family name",
                            msgTarget: "side",
                            bind: {
                                value: "{row.familyName}"
                            }
                        },
                        {
                            xtype: "textfield",
                            fieldLabel: "First name",
                            msgTarget: "side",
                            bind: {
                                value: "{row.firstName}"
                            }
                        },
                        {
                            xtype: "textfield",
                            fieldLabel: "Last name",
                            msgTarget: "side",
                            bind: {
                                value: "{row.lastName}"
                            }
                        },
                        {
                            xtype: "combobox",
                            fieldLabel: "Gender",
                            msgTarget: "side",
                            displayField: "display",
                            valueField: "code",
                            store: {
                                data: [
                                    { code: "", display: ""},
                                    { code: "male", display: "Male"},
                                    { code: "female", display: "Female"},
                                    { code: "other", display: "Other"},
                                    { code: "unknown", display: "Unknown"},
                                ]
                            },
                            bind: {
                                value: "{row.gender}"
                            }
                        },
                        {
                            xtype: "datefield",
                            fieldLabel: "Birth date/time",
                            format: "d.m.Y",
                            altFormats: "c",
                            bind: {
                                value: "{row.birthDate}"
                            }
                        },
                        {
                            xtype: "checkbox",
                            fieldLabel: "Active",
                            bind: {
                                value: "{row.active}"
                            }
                        }
                    ]
                },
                {
                    title: "Photo",
                    layout: "fit",
                    tbar: [
                        {
                            text: "Load",
                            handler: "onCommandLoadPhoto"
                        },
                        {
                            text: "Delete",
                            handler: "onCommandDeletePhoto"
                        }
                    ],
                    items: [
                        {
                            xtype: "image",
                            reference: "imgPhoto",
                            bind: {
                                src: "{row.photo}"
                            },
                            src: "about:blank",
                            border: 1,
                            style: {
                                borderColor: "#444444"
                            }
                        }
                    ],
                    bind: {
                        disabled: "{!row.uuid}"
                    }
                },
                {
                    title: "Additional names",
                    xtype: "grid",
                    bind: {
                        store: "{names}",
                        selection: "{selectedName}",
                        disabled: "{!row.uuid}"
                    },
                    listeners: {
                        rowdblclick: "onCommandEditName"
                    },
                    tbar: [
                        {
                            text: "Add",
                            handler: "onCommandAddName"
                        },
                        {
                            text: "Edit",
                            handler: "onCommandEditName",
                            disabled: true,
                            bind: {
                                disabled: "{!selectedName}"
                            }
                        },
                        {
                            text: "Delete",
                            handler: "onCommandDeleteName",
                            disabled: true,
                            bind: {
                                disabled: "{!selectedName}"
                            }
                        },
                        {
                            text: "Refresh",
                            handler: function() {
                                this.up("grid").getStore().reload();
                            }
                        }
                    ],
                    columns: [
                        {
                            text: "Family name",
                            flex: 1,
                            dataIndex: "familyName"
                        },
                        {
                            text: "First name",
                            flex: 1,
                            dataIndex: "firstName"
                        },
                        {
                            text: "Last name",
                            flex: 1,
                            dataIndex: "lastName"
                        },
                        {
                            text: "Prefix",
                            flex: 0.5,
                            dataIndex: "prefix"
                        },
                        {
                            text: "Suffix",
                            flex: 0.5,
                            dataIndex: "suffix"
                        },
                        {
                            dataIndex: "uuid",
                            width: 3
                        }
                    ]
                },
                {
                    title: "Addresses",
                    xtype: "grid",
                    tbar: [
                        {
                            text: "Refresh",
                            handler: function() {
                                this.up("grid").getStore().reload();
                            }
                        }
                    ],
                    columns: [
                        {
                            text: "Country",
                            flex: 1,
                            dataIndex: "country"
                        },
                        {
                            text: "City",
                            flex: 1,
                            dataIndex: "city"
                        },
                        {
                            text: "Address",
                            flex: 1,
                            dataIndex: "line"
                        },
                        {
                            dataIndex: "uuid",
                            width: 3
                        }
                    ],
                    bind: {
                        disabled: "{!row.uuid}",
                        store: "{addresses}"
                    }
                },
                {
                    title: "Contacts",
                    xtype: "grid",
                    tbar: [
                        {
                            text: "Refresh",
                            handler: function() {
                                this.up("grid").getStore().reload();
                            }
                        }
                    ],
                    columns: [
                        {
                            text: "Use",
                            flex: 1,
                            dataIndex: "use"
                        },
                        {
                            text: "System",
                            flex: 1,
                            dataIndex: "system"
                        },
                        {
                            text: "Value",
                            flex: 1,
                            dataIndex: "value"
                        },
                        {
                            dataIndex: "uuid",
                            width: 3
                        }
                    ],
                    bind: {
                        store: "{contacts}",
                        disabled: "{!row.uuid}"
                    }
                }
            ]
        }
    ],

    buttons: [
        {
            text: "OK",
            handler: "onOK",
            bind: {
                disabled: "{!formValid}"
            }
        },
        {
            text: "Cancel",
            handler: "onCancel"
        }
    ]
});
