Ext.define("Uhc.view.patients.PatientSearchForm", {
    extend: "Ext.form.Panel",
    modal: true,
    closable: true,
    floating: true,
    baseCls: Ext.baseCSSPrefix + 'window',
    width: 600,
    layout: "form",
    title: "Search patients",
    bodyPadding: 5,
    defaultFocus: "#txtPatientId",
    viewModel: {
        data: {
            row: {
                id: undefined,
                familyName: undefined,
                firstName: undefined,
                lastName: undefined,
                birthDateFrom: undefined,
                birthDateTo: undefined
            }
        }
    },
    keyMap: {
        ENTER: {
            handler: function(e, view) {
                view.fireEvent("ok");
            }
        }
    },
    items: [
        {
            xtype: "textfield",
            fieldLabel: "Patient ID",
            itemId: "txtPatientId",
            name: "id",
            bind: {
                value: "{row.id}"
            }
        },
        {
            xtype: "textfield",
            fieldLabel: "Family name",
            name: "familyName",
            bind: {
                value: "{row.familyName}"
            }
        },
        {
            xtype: "textfield",
            fieldLabel: "First name",
            name: "firstName",
            bind: {
                value: "{row.firstName}"
            }
        },
        {
            xtype: "datefield",
            format: "d.m.Y",
            name: "birthDateFrom",
            submitFormat: "c",
            fieldLabel: "Birth date (from)",
            bind: {
                value: "{row.birthDateFrom}"
            }
        },
        {
            xtype: "datefield",
            format: "d.m.Y",
            name: "birthDateTo",
            submitFormat: "c",
            fieldLabel: "Birth date (to)",
            bind: {
                value: "{row.birthDateTo}"
            }
        }
    ],
    buttons: [
        {
            xtype: "label",
            text: "Use the '*' symbol as a wildcard"
        },
        {
            text: "Search",
            handler: function() {
                this.up("form").fireEvent("ok");
            }
        },
        {
            text: "Clear",
            handler: function() {
                this.up("form").reset();
                this.up("form").fireEvent("clear");
            }
        },
        {
            text: "Cancel",
            handler: function() {
                this.up("form").hide();
            }
        }
    ]
});