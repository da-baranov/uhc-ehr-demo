Ext.define('Uhc.view.patients.PatientsModel', {
    requires: [
        "Uhc.store.Patient",
        "Uhc.model.PatientSearch"
    ],
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.patients',
    data: {
        selection: undefined,
        search: Ext.create("Uhc.model.PatientSearch")
    },

    stores: {
        patients: {
            type: "patient",
            listeners: {
                load: "onStoreLoad"
            }
        }
    }
});
