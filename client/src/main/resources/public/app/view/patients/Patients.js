Ext.define('Uhc.view.patients.Patients', {
    extend: 'Ext.grid.Panel',
    alias: "widget.patients",
    requires: [
        'Uhc.view.patients.PatientsController',
        'Uhc.view.patients.PatientsModel'
    ],

    controller: 'patients',
    viewModel: {
        type: 'patients'
    },

    title: "Patients",
    selModel: "checkboxmodel",

    bind: {
        store: "{patients}",
        selection: "{selection}"
    },

    keyMap: {
        INSERT: {
            handler: "onCommandAdd"
        },
        ENTER: {
            handler: "onCommandEdit"
        },
        DELETE: {
            handler: "onCommandDelete"
        }
    },

    listeners: {
        rowdblclick: "onCommandEdit"
    },

    tbar: [
        {
            text: "Add",
            tooltip: "Register a patient",
            iconCls: "fa fa-plus",
            handler: "onCommandAdd"
        },
        {
            text: "Edit",
            tooltip: "Edit patient info",
            iconCls: "fa fa-edit",
            handler: "onCommandEdit",
            bind: {
                disabled: "{!selection}"
            }
        },
        {
            text: "Delete",
            tooltip: "Delete selected records",
            iconCls: "fa fa-trash",
            handler: "onCommandDelete",
            bind: {
                disabled: "{!selection}"
            }
        },
        {
            text: "Refresh",
            tooltip: "Refresh data",
            handler: "onCommandRefresh"
        },
        '-',
        {
            text: "Search",
            tooltip: "Search",
            iconCls: "fa fa-search",
            handler: "onCommandSearch"
        },
        {
            text: "Reset",
            tooltip: "Reset",
            iconCls: "fa fa-search-minus",
            handler: "onCommandResetSearch"
        },
        '-',
        {
            text: "Seed",
            tooltip: "Add some data",
            iconCls: "fa fa-table",
            handler: "onCommandSeed"
        },
        {
            text: "Clear",
            tooltip: "Delete all records",
            iconCls: "fa fa-fire",
            handler: "onCommandClear"
        }
    ],

    columns: [
        {
            text: "Patient ID",
            flex: 1,
            dataIndex: "id"
        },
        {
            text: "Family Name",
            flex: 2,
            dataIndex: "familyName"
        },
        {
            text: "First Name",
            flex: 1,
            dataIndex: "firstName"
        },
        {
            text: "Birth Date",
            xtype: "datecolumn",
            flex: 1,
            format: "d.m.Y",
            dataIndex: "birthDate"
        },
        {
            text: "Age",
            xtype: "numbercolumn",
            width: 40,
            dataIndex: "age",
            format: "0"
        },
        {
            text: "Gender",
            flex: 1,
            dataIndex: "gender"
        },
        {
            text: "Active",
            width: 60,
            xtype: "checkcolumn",
            dataIndex: "active",
            listeners: {
                beforecheckchange: function() {
                    return false;
                }
            }
        }
    ],

    dockedItems: [{
        xtype: 'pagingtoolbar',
        bind: {
            store: "{patients}"
        },
        dock: 'bottom',
        displayInfo: true,
        beforePageText: 'Page',
        afterPageText: ' / {0}',
        displayMsg: 'Patients {0} - {1} of {2}'
    }],
});
