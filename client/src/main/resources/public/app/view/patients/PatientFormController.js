Ext.define('Uhc.view.patients.PatientFormController', {
    requires: [
        "Uhc.Util",
        "Uhc.Ajax",
        "Uhc.Constants",
        "Uhc.view.Name"
    ],
    extend: 'Ext.app.ViewController',
    alias: 'controller.patientform',

    bindings: {
        onRowSet: "{row}"
    },

    getUuid: function() {
        return this.getViewModel().get("row.uuid");
    },

    createRecord: function() {
        const model = Ext.create("Uhc.model.Patient");
        model.set("uuid", undefined);
        this.getViewModel().set("row", model);
    },

    editRecord: function(patientId) {
        const view = this.getView();
        const viewModel = this.getViewModel();
        const model = Ext.create("Uhc.model.Patient");
        model.set("uuid", patientId);

        model.load({
            callback: function(record, operation, success) {
                view.unmask();
                if (!success) {
                    Uhc.Util.errorMessageBox(operation);
                    view.close();
                } else {
                    viewModel.set("row", model);
                }
            }
        });
    },

    onValidityChange: function(sender, valid) {
        // this.getViewModel().set("formValid", valid);
        this.getViewModel().set("formValid", true);
    },

    onOK: async function() {
        try {
            const view = this.getView();
            if (!view.isValid()) {
                Uhc.MessageBox.alert("Something went wrong", "Please provide the correct information.")
                return;
            }
            const viewModel = this.getViewModel();
            const row = viewModel.get("row");
            row.save({
                callback: function(record, operation, success) {
                    if (!success) {
                        Uhc.Util.errorMessageBox(operation);
                    } else {
                        view.fireEvent("ok", row); // pass model to the caller on success
                    }
                }
            });
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e);
        }
    },

    onCancel: function() {
        const view = this.getView();
        view.fireEvent("cancel");
    },

    onStoreLoad: function(sender, records, success, operation) {
        if (!success) {
            Uhc.Util.errorMessageBox(operation);
        }
    },

    onCommandLoadPhoto: async function() {
        try {
            const uuid = this.getUuid();
            const file = await Uhc.Util.loadPictureAsFile();
            if (!file) return;

            var formData = new FormData();
            formData.append("file", file);
            const url = `${Uhc.Constants.API_PATIENTS}/${uuid}/photo`;
            const response = await window.fetch(url, {
                method: "POST",
                body: formData
            });
            if (!response.ok) {
                throw new Error("Failed to upload patient photo");
            }
            this.loadPatientPhoto(uuid);
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e);
        }
    },

    onCommandDeletePhoto: async function() {
        try {
            const mr = await Uhc.MessageBox.confirm("Question", "Delete photo?");
            if (mr !== "yes") return;

            const uuid = this.getUuid();
            const url = `${Uhc.Constants.API_PATIENTS}/${uuid}/photo`;
            await Uhc.Ajax.delete(url, {});
            this.loadPatientPhoto(uuid);
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Failed to delete patient photo");
        }
    },

    loadPatientPhoto: function(uuid) {
        const src = `${Uhc.Constants.API_PATIENTS}/${uuid}/photo`;
        this.lookup("imgPhoto").setSrc("about:blank");
        this.lookup("imgPhoto").setSrc(src);
    },

    loadPatientNames: function(uuid) {
        if (uuid) {
            const storeNames = this.getStore("names");
            storeNames.getProxy().setUrl(`${Uhc.Constants.API_PATIENTS}/${uuid}/name`);
            storeNames.load();
        }
    },

    loadPatientAddresses: function(uuid) {
        if (uuid) {
            const storeAddresses = this.getStore("addresses");
            storeAddresses.getProxy().setUrl(`${Uhc.Constants.API_PATIENTS}/${uuid}/address`);
            storeAddresses.load();
        }
    },

    loadPatientContacts: function(uuid) {
        if (uuid) {
            const storeContacts = this.getStore("contacts");
            storeContacts.getProxy().setUrl(`${Uhc.Constants.API_PATIENTS}/${uuid}/contact`);
            storeContacts.load();
        }
    },

    onRowSet: function(model) {
        if (!model) return;

        const uuid = model.get("uuid");
        if (uuid) {
            this.loadPatientNames(uuid);
            this.loadPatientAddresses(uuid);
            this.loadPatientContacts(uuid);
            this.loadPatientPhoto(uuid);
        }
    },

    onCommandAddName: function() {
        try {
            const me = this;
            const uuid = this.getUuid();
            const form = Ext.create("Uhc.view.Name");
            form.getController().createRecord(uuid);
            form.on("ok", function () {
                form.close();
                me.getStore("names").reload();
            });
            form.show();
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Failed to add patient name");
        }
    },

    onCommandEditName: function() {
        try {
            const me = this;
            const uuid = this.getUuid();
            const nameUuid = this.getViewModel().get("selectedName.uuid");
            if (!nameUuid) return;
            const form = Ext.create("Uhc.view.Name");
            form.on("ok", function () {
                form.close();
                me.getStore("names").reload();
            });
            form.getController().editRecord(uuid, nameUuid);
            form.show();
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Failed to edit patient name");
        }
    },

    onCommandDeleteName: async function() {
        try {
            const me = this;
            const uuid = this.getUuid();
            const selectedName = this.getViewModel().get("selectedName");
            if (selectedName == null) return;
            const url = `${Uhc.Constants.API_PATIENTS}/${uuid}/name`;
            selectedName.getProxy().setUrl(url);

            const rv = await Uhc.MessageBox.confirm("Question", "Delete this name?");
            if (rv !== "yes") return;

            selectedName.erase({
                callback: function(record, operation, success) {
                    if (!success) {
                        Uhc.Util.errorMessageBox(operation, "Failed to delete this patient name.");
                    }
                    me.getStore("names").reload();
                }
            })
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Failed to delete patient name");
        }
    }
});
