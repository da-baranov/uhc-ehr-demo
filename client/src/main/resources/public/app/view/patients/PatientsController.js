Ext.define('Uhc.view.patients.PatientsController', {
    requires: [
        "Uhc.Util",
        "Uhc.Ajax",
        "Uhc.MessageBox",
        "Uhc.view.patients.PatientForm",
        "Uhc.view.patients.PatientSearchForm",
        "Uhc.model.Patient",
        "Uhc.model.PatientSearch"
    ],
    extend: 'Ext.app.ViewController',
    alias: 'controller.patients',

    searchForm: undefined,

    constructor: function() {
        this.callParent(arguments);
    },

    onStoreLoad: function(sender, records, successful, operation) {
        if (!successful) {
            Uhc.Util.errorMessageBox(operation);
        } else {
            if (records && records.length) {
                this.getViewModel().set("selection", records[0]);
            } else {
                this.getViewModel().set("selection", undefined);
            }
        }
    },

    onCommandAdd: function () {
        try {
            const form = Ext.create("Uhc.view.patients.PatientForm");
            const viewModel = this.getViewModel();

            form.getController().createRecord();

            const store = this.getStore("patients");
            form.on("ok", function (row) {
                store.add(row);
                viewModel.set("selection", row);
                form.close();
            });
            form.on("cancel", function () {
                form.close();
            });
            form.show();
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Cannot add a record.");
        }
    },

    onCommandEdit: function() {
        try {
            const currentRow = this.getViewModel().get("selection");
            if (!currentRow) return;

            const form = Ext.create("Uhc.view.patients.PatientForm");
            form.getController().editRecord(currentRow.get("uuid"));

            form.on("ok", function () {
                form.close();
                currentRow.load(currentRow.get("uuid"), {
                    failure: function (record, operation) {
                        Uhc.Util.errorMessageBox(operation, "Failed to refresh record.");
                    }
                });
            });
            form.on("cancel", function () {
                form.close();
            });
            form.show();
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Cannot edit this record.");
        }
    },

    onCommandDelete: async function() {
        const selection = this.getView().getSelection();
        if (!Array.isArray(selection) || selection.length === 0) return;

        const mr = await Uhc.MessageBox.confirm("Question", "Remove selected records?");
        if (mr !== "yes") return;

        try {
            for (var i = 0; i < selection.length; i++) {
                const model = selection[i];
                const uuid = model.get("uuid");
                const url = `${Uhc.Constants.API_PATIENTS}/${uuid}`;
                const rv = await Uhc.Ajax.delete(url);
            }
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Cannot delete records.");
        }
        finally {
            this.getStore("patients").reload();
        }
    },

    onCommandRefresh: function() {
        const store = this.getStore("patients");
        store.reload();
    },

    onCommandSearch: function() {
        try {
            const me = this;
            if (!me.searchForm) {
                me.searchForm = Ext.create("Uhc.view.patients.PatientSearchForm", {
                    closeAction: "hide"
                });
            }
            me.searchForm.on("ok", async function () {
                const data = me.searchForm.getForm().getValues();
                me.doSearch(data);
                me.searchForm.hide();
            });
            me.searchForm.on("clear", async function () {
                const data = me.searchForm.getForm().getValues();
                me.doSearch(data);
                me.searchForm.hide();
            });
            me.searchForm.show();
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e);
        }
    },

    onCommandResetSearch: async function() {
          await this.doSearch({});
    },

    doSearch: async function(args) {
        const me = this;
        const store = me.getStore("patients");
        store.getProxy().setExtraParams(args);
        store.loadPage(1);
    },

    onCommandSeed: async function() {
        const mr = await Uhc.MessageBox.confirm("Question", "This will add 1000 fake patients to the database. Continue?");
        if (mr !== "yes") return;

        const view = this.getView();
        try {
            view.mask("Seeding 1000 records...");
            const response = await window.fetch("/api/v1/patient/seed", {
                method: "POST"
            });
            const text = await response.text();
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Cannot seed data.");
        }
        finally {
            view.unmask();
            view.getStore().reload();
        }
    },

    onCommandClear: async function() {
        const mr = await Uhc.MessageBox.confirm("Question", "Do you want to delete all data?");
        if (mr !== "yes") return;
        const view = this.getView();
        try {
            view.mask("Deleting records... please wait.");
            const response = await window.fetch("/api/v1/patient/clear", {
                method: "POST"
            });
            const text = await response.text();
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e, "Cannot clear data.");
        }
        finally {
            view.unmask();
            view.getStore().reload();
        }
    }
});
