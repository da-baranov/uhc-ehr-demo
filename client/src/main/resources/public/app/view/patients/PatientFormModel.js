Ext.define('Uhc.view.patients.PatientFormModel', {
    requires: [
        "Uhc.model.HumanName",
        "Uhc.model.PersonAddress",
        "Uhc.model.ContactPoint"
    ],
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.patientform',
    data: {
        row: undefined,
        selectedName: undefined
    },

    stores: {
        names: {
            model: "Uhc.model.HumanName",
            autoLoad: false,
            proxy: {
                type: "ajax",
                url: "will-be-set-dynamically-later",
                reader: {
                    type: "json",
                    rootProperty: "data"
                }
            },
            listeners: {
                load: "onStoreLoad"
            }
        },

        addresses: {
            model: "Uhc.model.PersonAddress",
            autoLoad: false,
            proxy: {
                type: "ajax",
                url: "will-be-set-dynamically-later",
                reader: {
                    type: "json",
                    rootProperty: "data"
                }
            },
            listeners: {
                load: "onStoreLoad"
            }
        },

        contacts: {
            model: "Uhc.model.ContactPoint",
            autoLoad: false,
            proxy: {
                type: "ajax",
                url: "will-be-set-dynamically-later",
                reader: {
                    type: "json",
                    rootProperty: "data"
                }
            },
            listeners: {
                load: "onStoreLoad"
            }
        }
    }
});
