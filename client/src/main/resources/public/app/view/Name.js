Ext.define('Uhc.view.Name',{
    extend: 'Ext.form.Panel',
    closable: true,
    modal: true,
    floating: true,
    baseCls: Ext.baseCSSPrefix + 'window',
    layout: "form",
    title: "Patient name",
    width: 400,
    height: 240,

    requires: [
        'Uhc.view.NameController',
        'Uhc.view.NameModel'
    ],
    modelValidation: true,
    controller: 'name',
    viewModel: 'name',
    layout: "form",
    bodyPadding: 4,

    items: [
        {
            xtype: "combobox",
            fieldLabel: "Use",
            allowBlank: false,
            displayField: "code",
            valueField: "code",
            store: {
                data: [
                    { code: "Usual" },
                    { code: "Official" },
                    { code: "Temp" },
                    { code: "Nickname" },
                    { code: "Anonymous" },
                    { code: "Old" },
                    { code: "Maiden" }
                ]
            },
            bind: "{row.use}",
            msgTarget: "side"
        },
        {
            xtype: "textfield",
            fieldLabel: "Family name",
            allowBlank: false,
            bind: "{row.familyName}",
            msgTarget: "side"
        },
        {
            xtype: "textfield",
            fieldLabel: "First name",
            bind: "{row.firstName}",
            msgTarget: "side"
        },
        {
            xtype: "textfield",
            fieldLabel: "Last name",
            bind: "{row.lastName}",
            msgTarget: "side"
        },
        {
            xtype: "textfield",
            fieldLabel: "Prefix",
            bind: "{row.prefix}",
            msgTarget: "side"
        },
        {
            xtype: "textfield",
            fieldLabel: "Suffix",
            bind: "{row.suffix}",
            msgTarget: "side"
        }
    ],

    buttons: [
        {
            text: "OK",
            handler: "onOK"
        },
        {
            text: "Cancel",
            handler: function() { this.up("form").close(); }
        }
    ]
});
