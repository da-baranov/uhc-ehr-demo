Ext.define('Uhc.view.NameController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.name',
    requires: [
        "Uhc.model.HumanName"
    ],

    createRecord: function(patientId) {
        const url = `${Uhc.Constants.API_PATIENTS}/${patientId}/name`;
        const row = Ext.create("Uhc.model.HumanName");
        row.set("uuid", undefined);
        row.getProxy().setUrl(url);
        this.getViewModel().set("row", row);
    },

    editRecord: function(patientId, nameId) {
        const view = this.getView();
        const viewModel = this.getViewModel();
        const url = `${Uhc.Constants.API_PATIENTS}/${patientId}/name`;
        const row = Ext.create("Uhc.model.HumanName");
        row.getProxy().setUrl(url);
        row.set("uuid", nameId);
        row.load({
           callback: function(record, operation, success) {
               if (!success) {
                   Uhc.Util.errorMessageBox(operation, "Failed to load record.");
                   view.close();
               } else {
                    viewModel.set("row", row);
               }
           }
        });
    },

    onOK: async function() {
        try {
            const view = this.getView();
            if (!view.isValid()) {
                Uhc.MessageBox.alert("Something went wrong", "Please provide the correct information.")
                return;
            }

            const viewModel = this.getViewModel();
            const row = viewModel.get("row");
            row.save({
                callback: function(record, operation, success) {
                    if (!success) {
                        Uhc.Util.errorMessageBox(operation, "Cannot save record.");
                    } else {
                        view.fireEvent("ok", row);
                    }
                }
            });
        }
        catch (e) {
            Uhc.Util.errorMessageBox(e);
        }
    },
});
