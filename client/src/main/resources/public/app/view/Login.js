Ext.define('Uhc.view.Login',{
    extend: 'Ext.form.Panel',
    closable: true,
    modal: true,
    floating: true,
    requires: [
        'Uhc.view.LoginController',
        'Uhc.view.LoginModel'
    ],
    controller: 'login',
    viewModel: 'login',
    baseCls: Ext.baseCSSPrefix + 'window',
    layout: "form",

    title: "UHC EHR Login",
    width: 400,
    height: 150,
    defaultFocus: "#txtPassword",
    bodyPadding: 10,

    listeners: {
        show: "onFormShow",
        validitychange: "onValidityChange",
    },

    items: [
        {
            xtype: "combobox",
            fieldLabel: "User ID",
            displayField: "userId",
            itemId: "txtUserId",
            valueField: "userId",
            allowBlank: false,
            value: "Administrator",
            store: {
                data: [
                    { userId: "Administrator" },
                    { userId: "Physician"}
                ]
            }
        },
        {
            xtype: "textfield",
            itemId: "txtPassword",
            inputType: "password",
            fieldLabel: "Password",
            allowBlank: false
        }
    ],

    buttons: [
        {
            text: "OK",
            bind: {
                disabled: "{!formValid}"
            }
        },
        {
            text: "Cancel",
            handler: function() {
                this.up("form").close();
            }
        }
    ]
});
