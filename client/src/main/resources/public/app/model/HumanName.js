Ext.define("Uhc.model.HumanName", {
    extend: "Ext.data.Model",

    idProperty: "uuid",

    fields: [
        {
            name: "uuid",
            type: "string"
        },
        {
            name: "familyName",
            type: "string"
        },
        {
            name: "firstName",
            type: "string"
        },
        {
            name: "lastName",
            type: "string"
        },
        {
            name: "prefix",
            type: "string"
        },
        {
            name: "suffix",
            type: "string"
        },
        {
            name: "use",
            type: "string"
        }
    ],

    validators: {
        familyName: "presence",
        use: "presence"
    },

    proxy: {
        type: "rest",
        url: Uhc.Constants.API_PATIENTS,
        reader: {
            type: "json"
        },
        writer: {
            type: "json",
            writeAllFields: true
        }
    }
})