Ext.define("Uhc.model.ContactPoint", {
    extend: "Ext.data.Model",

    idProperty: "_id",

    fields: [
        {
            name: "_id",
            type: "string"
        },
        {
            name: "uuid",
            type: "string"
        },
        {
            name: "system",
            type: "string"
        },
        {
            name: "use",
            type: "string"
        },
        {
            name: "value",
            type: "string"
        }
    ],

    validators: {
        value: "presence",
        use: "presence",
        system: "presence"
    }
});