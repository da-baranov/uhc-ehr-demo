Ext.define("Uhc.model.PersonAddress", {
    extend: "Ext.data.Model",

    idProperty: "_id",

    fields: [
        {
            name: "_id",
            type: "string"
        },
        {
            name: "uuid",
            type: "string"
        },
        {
            name: "city",
            type: "string"
        },
        {
            name: "country",
            type: "string"
        },
        {
            name: "district",
            type: "string"
        },
        {
            name: "line",
            type: "string"
        },
        {
            name: "postalCode",
            type: "string"
        },
        {
            name: "state",
            type: "string"
        },
        {
            name: "text",
            type: "string"
        },
        {
            name: "type",
            type: "string"
        },
        {
            name: "use",
            type: "string"
        }
    ],

    validators: {
        country: "presence",
        city: "presence",
        line: "presence"
    }
});