Ext.define("Uhc.model.PatientSearch", {
    extend: "Ext.data.Model",
    idProperty: "_id",
    fields: [
        { name: "_id", type: "string" },
        { name: "familyName", type: "string" },
        { name: "firstName", type: "string" },
        { name: "lastName", type: "string" },
        { name: "birthDateFrom", type: "date" },
        { name: "birthDateTo", type: "date" }
    ]
})