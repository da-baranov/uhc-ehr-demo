Ext.define("Uhc.model.Patient", {
    extend: "Ext.data.Model",

    idProperty: "uuid",

    fields: [
        {
            name: "uuid",
            type: "string"
        },
        {
            name: "id",
            type: "string"
        },
        {
            name: "familyName",
            type: "string"
        },
        {
            name: "firstName",
            type: "string"
        },
        {
            name: "lastName",
            type: "string"
        },
        {
            name: "gender",
            type: "string"
        },
        {
            name: "birthDate",
            type: "date",
            dateFormat: "c"
        },
        {
            name: "age",
            type: "number"
        },
        {
            name: "active",
            type: "boolean",
            defaultValue: true
        }
    ],

    validators: {
        id: {
            type: "presence"
        },
        familyName: {
            type: "presence"
        }
    },

    proxy: {
        type: "rest",
        url: Uhc.Constants.API_PATIENTS,
        reader: {
            type: "json"
        },
        writer: {
            type: "json",
            writeAllFields: true
        }
    }
})