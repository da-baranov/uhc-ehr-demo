package org.uhc.ehr.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.uhc.ehr.data.dto.Patient;
import org.uhc.ehr.data.service.PatientRequest;
import org.uhc.ehr.data.service.PatientService;
import org.uhc.ehr.data.service.SeedingService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Slf4j
public class IntegrationTests {

    final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    MockMvc mockMvc;

    @Autowired
    PatientService patientService;

    @Autowired
    SeedingService seedingService;

    private void out(String message) {
        System.out.println("----------------------------------------------------------------------");
        System.out.println(message);
        System.out.println("----------------------------------------------------------------------");
    }

    @Test
    @Order(1)
    @Transactional
    @Rollback(value = false)
    public void testSeeding() {
        seedingService.seedDictionaries();
        seedingService.seedPatients(10);

        out("testSeeding PASSED");
    }

    @Test
    @Transactional
    @Order(2)
    public void testWhenSelectPatients_thenReturn200() throws Throwable {
        var builder = get("/api/v1/patient");
        var response =
                mockMvc.perform(builder)
                .andExpect(status().isOk());

        out("testSelectPatients PASSED");
    }

    @Test
    @Transactional
    @Order(3)
    public void testWhenPatientInserted_thenReturn201() throws Throwable {
        var faker = new Faker();

        for (var i = 0; i < 10; i++) {
            var patient = new Patient();
            patient.setId(faker.idNumber().ssnValid());
            patient.setFamilyName(faker.name().lastName());
            patient.setFirstName(faker.name().firstName());

            mockMvc.perform(post("/api/v1/patient")
                    .content(objectMapper.writeValueAsString(patient))
                    .contentType("application/json"))
                    .andExpect(status().isCreated());
        }

        out("testInsertPatients PASSED");
    }

    @Test
    @Transactional
    @Order(4)
    public void testWhenPatientUpdated_thenReturn200() throws Throwable {
        var patientRequest = new PatientRequest();
        patientRequest.set_count(10);
        var patients = patientService.getPatients(patientRequest).getData();
        for (var patient : patients) {
            patient.setActive(!patient.getActive());
            mockMvc.perform(put("/api/v1/patient")
                    .content(objectMapper.writeValueAsString(patient))
                    .contentType("application/json"))
                    .andExpect(status().isOk());
        }

        out("testUpdatePatients PASSED");
    }

    @Test
    @Transactional
    @Order(5)
    public void testWhenPatientDeleted_thenReturn200() throws Throwable {
        var patientRequest = new PatientRequest();
        patientRequest.set_count(10);
        var patients = patientService.getPatients(patientRequest).getData();
        for (var patient : patients) {
            mockMvc.perform(delete("/api/v1/patient/" + patient.getUuid().toString())
                    .contentType("application/json"))
                    .andExpect(status().isOk());
        }

        out("testDeletePatients PASSED");
    }

    @Test
    public void testWhenPatientInvalid_thenReturn400() throws Throwable {
        var patientJson = objectMapper.writeValueAsString(new Patient()); // no id and no family name
        mockMvc.perform(post("/api/v1/patient")
                .contentType("application/json")
                .content(patientJson))
                .andExpect(status().isBadRequest());
    }
}
