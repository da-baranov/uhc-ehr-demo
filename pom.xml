<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.7.3</version>
		<relativePath/>
	</parent>
	<groupId>org.uhc</groupId>
	<artifactId>ehr</artifactId>
	<name>EHR</name>
	<version>0.0.1-SNAPSHOT</version>
	<description>UHC EHR demo</description>

	<packaging>pom</packaging>

	<properties>
		<maven.compiler.target>11</maven.compiler.target>
		<maven.compiler.source>11</maven.compiler.source>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

		<ehcache.version>2.6.11</ehcache.version>
		<hibernate.version>5.6.10.Final</hibernate.version>
		<junit>4.13.2</junit>
		<lombok.version>1.18.24</lombok.version>
		<postgres.version>42.5.0</postgres.version>
		<org.springframework.boot>2.7.3</org.springframework.boot>

		<spring.security.version>5.7.3</spring.security.version>
		<spring.test>5.3.23</spring.test>
		<modelmapper.version>3.1.0</modelmapper.version>
		<springdoc.version>1.6.11</springdoc.version>
		<snakeyaml.version>1.32</snakeyaml.version>
		<junit-jupiter-engine.version>5.5.2</junit-jupiter-engine.version>
		<maven-compiler-plugin.version>3.10.1</maven-compiler-plugin.version>
		<maven-war-plugin.version>3.3.2</maven-war-plugin.version>
		<versions-maven-plugin.version>2.11.0</versions-maven-plugin.version>
		<springdoc-openapi-ui.version>1.6.11</springdoc-openapi-ui.version>
	</properties>

	<modules>
		<module>data</module>
		<module>rest</module>
		<module>client</module>
    </modules>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-validation</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
			<optional>true</optional>
		</dependency>

		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>

		<dependency>
			<groupId>org.modelmapper</groupId>
			<artifactId>modelmapper</artifactId>
			<version>${modelmapper.version}</version>
		</dependency>

		<!-- JAVAFAKER BEGIN -->
		<dependency>
			<groupId>com.github.javafaker</groupId>
			<artifactId>javafaker</artifactId>
			<version>1.0.2</version>
			<exclusions>
				<exclusion>
					<groupId>org.yaml</groupId>
					<artifactId>snakeyaml</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>org.yaml</groupId>
			<artifactId>snakeyaml</artifactId>
			<version>${snakeyaml.version}</version>
		</dependency>
		<!-- JAVAFAKER END -->

		<dependency>
			<groupId>org.springdoc</groupId>
			<artifactId>springdoc-openapi-ui</artifactId>
			<version>${springdoc.version}</version>
		</dependency>
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.uhc</groupId>
				<artifactId>client</artifactId>
				<version>${project.version}</version>
				<scope>compile</scope>
			</dependency>

			<dependency>
				<groupId>org.uhc</groupId>
				<artifactId>rest</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>org.uhc</groupId>
				<artifactId>data</artifactId>
				<version>${project.version}</version>
			</dependency>

			<dependency>
				<groupId>org.postgresql</groupId>
				<artifactId>postgresql</artifactId>
				<version>${postgres.version}</version>
			</dependency>

			<dependency>
				<groupId>org.springdoc</groupId>
				<artifactId>springdoc-openapi-ui</artifactId>
				<version>${springdoc-openapi-ui.version}</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-maven-plugin</artifactId>
					<version>2.7.3</version>
					<configuration>
						<excludes>
							<exclude>
								<groupId>org.projectlombok</groupId>
								<artifactId>lombok</artifactId>
							</exclude>
						</excludes>
					</configuration>
				</plugin>
			</plugins>
		</pluginManagement>

		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${maven-compiler-plugin.version}</version>
				<configuration>
					<source>11</source>
					<target>11</target>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-war-plugin</artifactId>
				<version>${maven-war-plugin.version}</version>
				<configuration>
					<failOnMissingWebXml>false</failOnMissingWebXml>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.22.2</version>
				<dependencies>
					<dependency>
						<groupId>org.junit.jupiter</groupId>
						<artifactId>junit-jupiter-engine</artifactId>
						<version>${junit-jupiter-engine.version}</version>
					</dependency>
				</dependencies>
			</plugin>

			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>versions-maven-plugin</artifactId>
				<version>${versions-maven-plugin.version}</version>
			</plugin>
		</plugins>
	</build>
</project>
